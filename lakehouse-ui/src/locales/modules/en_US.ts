/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

const userDropdown = {
  profile: 'Profile',
  password: 'Password',
  logout: 'Logout'
}

const menu = {
  home: 'Home',
  instance: 'Instance',
  synchronization: 'Data Sync',
  explore: 'Explore',
  sqlconsole: 'SQL Console',
  user: 'User',
  data_manage: 'Data Manage',
  database: 'Database',
  directory: 'Data Directory',
  discovery: 'Data Discovery'
}

const common = {
  positiveText: 'Confirm',
  negativeText: 'Cancel',
  format: 'Format',
  excution: 'Excution',
  instance: 'Instance',
  refresh: 'Refresh',
  search: 'Search',
  delete: 'Delete',
  create: 'Create',
  action: 'Action',
  reset: 'Reset',
  min: 'Min',
  hour: 'Hour',
  day: 'Day',
  week: 'Week',
  month: 'Month',
  help_center: 'Docs'
}

const home = {
  welcome: 'welcome',
  instance_number: 'Instance Number',
  sets: 'sets',
  view_instance_list: 'View Instance List',
  today_job_number: 'Job Number of Today',
  view_job_monitor: 'View Job Monitor',
  use_guide: 'Use Guide',
  initialize_instance: 'Initialize Instance',
  create_instance: 'Create Instance',
  create_instance_desc: 'Select computing engines and create instances',
  bulid_datalake: 'Bulid Datalake',
  create_database: 'Create Database',
  create_database_desc: 'Create a database for target data management',
  create_now: 'Create Now',
  data_sync: 'Data Synchronization',
  data_sync_desc:
    'New data entering lake/warehouse scenario, data connect-task create-task monitor',
  one_click_sync: 'One Click Sync',
  metadata_discovery: 'Metadata Discovery',
  metadata_discovery_desc:
    'Object storage inventory data discovery scenario, task create-task monitor',
  discovry_now: 'Discovry Now',
  data_directory: 'Data Directory',
  data_directory_desc:
    'View and manage the constructed data Lake/warehouse data directory',
  view_data_directory: 'View Data Directory',
  data_explore: 'Data Explore',
  sql_console: 'SQL Console',
  sql_console_desc: 'Data analysis using spark SQL'
}

const pagination = {
  total: '',
  record: 'records in total'
}

const instance = {
  instance_list: 'Instance List',
  instance_name: 'Instance Name',
  engine_type: 'Engine Type',
  compute_resource_size: 'Compute Resource Size',
  status: 'Status',
  duration: 'Duration',
  create_user: 'Create User',
  create_time: 'Create Time',
  create: 'Create',
  manage: 'Manage',
  release: 'Release',
  delete_tips: 'Is Release Instance ',
  delete_tips_desc:
    'All data in the instance will be cleared and cannot be recovered. Please be careful.',
  create_modal: {
    size: 'Size',
    scene: 'Applicable Scene',
    cancel: 'Cancel',
    submit: 'Submit',
    instance_name_tips: 'Please enter the instance name',
    instance_name_format_tips:
      'Only the beginning of a letter is allowed, the length is limited to 4 ~ 32 characters, and can contain lowercase letters, numbers and underscores',
    compute_resource_tips:
      'The size of computing resources determines the query performance. You can enjoy this part of resources exclusively'
  },
  base_info: {
    base_info: 'Base Info',
    instance_name: 'Instance Name',
    instance_id: 'Instance ID',
    status: 'Status',
    engine_type: 'Engine Type',
    compute_resource_size: 'Compute Resource Size',
    create_user: 'Create User',
    create_time: 'Create Time'
  },
  system_config: {
    system_config: 'System Config',
    system_config_tips1:
      '1. The new value does not affect the currently running job and will take effect when the new job starts to execute；',
    system_config_tips2:
      '2.Reducing the maximum concurrent number of jobs can improve the execution efficiency of a single job, and increasing it may lead to job execution failure. Please be careful.',
    max_parallelize_num: 'Maximun Job Parallelize Number'
  },
  status_types: {
    running: 'running',
    creating: 'creating',
    create_failed: 'create failed',
    error: 'error',
    resizing: 'resizing',
    accepted: 'accepted'
  },
  size_types: {
    small: 'small（16CU）',
    medium: 'medium（64CU）',
    large: 'large（128CU）',
    xlarge: 'X-large（256CU）'
  },
  size_desc: {
    small:
      'Suitable for initial order, small data volume and low performance requirements',
    medium:
      'Moderate specification, suitable for scenarios with data volume less than 30tb',
    large:
      'Recommended configuration for medium and large enterprises, suitable for scenarios with data volume of 30-50tb',
    xlarge:
      'High performance configuration, suitable for scenarios with data volume of 50-100tb'
  }
}

const synchronization = {
  job_name: 'Job Name',
  job_id: 'Job ID',
  schedule_status: 'Schedule Status',
  create_user: 'Create User',
  create_time: 'Create Time',
  job_type: 'Job Type',
  create_form: {
    step1_title: 'Job Type',
    step2_title: 'Basic Config Info',
    step3_title: 'Source and Target Config',
    step4_title: 'Job Config',
    step5_title: 'Process Confirmation',
    next_step: 'Next',
    RDB_ALL_label: 'Offline synchronization of relational database',
    RDB_ALL_desc:
      'Synchronize the whole database or some tables of the specified relational database to the target built-in storage or object storage at one time or periodically. It is recommended to implement it during the low peak period of business. The currently supported databases include mysql',
    MQ_REALTIME_label: 'Message queue real time synchronization',
    MQ_REALTIME_desc:
      'Synchronize the data in message queues such as Kafka and pulsar to the target built-in storage or object storage in real time. Currently, only message queue Kafka is supported',
    job_desc: 'Job Description',
    data_source_config: 'Data Source Config',
    connector_group: 'Connector Group',
    connector_type: 'Connector Type',
    connector_id: 'Connector ID',
    database_name: 'Database Name',
    table: 'Table',
    table_name: 'Table Name',
    data_target_config: 'Data Target Config',
    table_prefix: 'Table Prefix',
    store_type: 'Storage Type',
    connector_path: 'Connect Path',
    ak: 'Access Key(AK)',
    sk: 'Secret Key(SK)',
    store_path: 'Storage Path',
    partition: 'Partition',
    schedule_model: 'Schedule Model',
    once: 'Once',
    cycle: 'Cycle',
    schedule_time: 'Schedule Time',
    duration_value: 'Duration Value',
    schedule_start_time: 'Schedule Start Time',
    schedule_end_time: 'Schedule End Time',
    fail_action: 'Task Fail Action',
    show_advanced_config: 'Show Advanced Config',
    hide_advanced_config: 'Hide Advanced Config',
    concurrence: 'Concurrence',
    type: 'Type',
    relational_database: 'Relational Database',
    all: 'All',
    multi: 'Multi'
  }
}

const directory = {
  table_name: 'Table Name',
  database_name: 'Database Name',
  location: 'Location',
  ref_task_type: 'Ref Task Type',
  ref_task_id: 'Ref Task ID',
  creator: 'Creator',
  create_time: 'Create Time',
  last_modifier: 'Last Modifier',
  last_modifier_time: 'Last Modifier Time',
  search_data: 'Search Data'
}

const user = {
  username: 'Username',
  password: 'Password',
  description: 'Description',
  addUser: 'Add User',
  delete_tips: 'Is delete user ',
  cancel: 'cancel',
  reset: 'reset',
  addModal: {
    username: 'username',
    password: 'password',
    passwordConfirm: 'password confirm',
    description: 'description',
    cancel: 'cancel',
    submit: 'submit',
    username_tips: 'Please enter your username',
    username_format_tips:
      'The length is limited to 2 ~ 32 characters. Only letters, numbers, underscores and middle dashes are supported. Once the user name is submitted, it cannot be changed. Please fill in it carefully',
    password_tips: 'Please enter your password',
    password_format_tips:
      'The length is limited to 8 ~ 20 characters, including uppercase letters, lowercase letters, numbers and special characters, at least one of each kind. The special characters are !@#$%^&*()_+-=',
    confirm_password_tips: 'Please enter your confirm password',
    two_password_entries_are_inconsistent:
      'Two password entries are inconsistent'
  },
  resetModal: {
    password_tips: 'Please enter your password',
    password_format_tips:
      'The length is limited to 8 ~ 20 characters, including uppercase letters, lowercase letters, numbers and special characters, at least one of each kind. The special characters are !@#$%^&*()_+-=',
    confirm_password_tips: 'Please enter your confirm password',
    two_password_entries_are_inconsistent:
      'Two password entries are inconsistent'
  }
}

const explore = {
  removeTabTip:
    'Are you sure you want to close the current sql console window?',
  sqlEditorTip:
    'SQL is executed in parallel. If there are dependencies on SQL statements, please execute them separately.',
  exec_history_list_title: 'Show only the execution history of nearly 30 days',
  exec_history_taskId: 'task id',
  exec_history_submitTime: 'Execution start time',
  exec_history_sqlContent: 'Execute statement',
  exec_history_status: 'Execute status',
  exec_history_runTime: 'Execute duration (s)',
  exec_history_search: 'by library/table',
  exec_result_exec_tip: 'The current statement is executing',
  exec_result_abort_exec_tip:
    'The execution of SQL statement has been interrupted',
  exec_result_abort_tip: 'Abort',
  exec_result_submit_failed: 'Submit failed',
  exec_result_failed_msg: 'Error message: ',
  exec_result_execution_failed: 'Execution failed',
  exec_result_showlog: 'Show log',
  success_result_search_limit:
    'Each query can display up to 400 rows of records',
  success_result_run_success: '[message] successful execution',
  success_result_search_tip: 'Search by query result content',
  query_panel_grammar_url: 'Grammar docs',
  db_not_created: 'There is no database, please go to the database creation',
  db_not_selected: 'There is no content, please select database',
  db_table: 'Lib/table ',
  accepted_status: 'Accepted',
  submitted_status: 'Submitted',
  running_status: 'Running',
  submit_failed_status: 'Submit Failed',
  cancelled_status: 'Cancelled',
  failed_status: 'Failted',
  finished_status: 'Finished',
  lost_status: 'Lost',
  unknown_status: 'Unknown',
  return_database_page: ' Return to data database catalog page? ',
  return_database_page_tip:
    'After returning, the operation content of the current page will not be retained. Please confirm to return! ',
  execution_history: 'Execution history'
}

const database = {
  create_database: 'Create database ',
  search_database: 'Search by library/table name',
  null_database: 'No database'
}

export default {
  common,
  userDropdown,
  menu,
  home,
  instance,
  database,
  directory,
  user,
  explore,
  synchronization,
  pagination
}
