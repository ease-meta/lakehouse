/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineStore } from 'pinia'
import { LocalesStore, Locales } from './types'

export const useLocalesStore = defineStore({
  id: 'locales',
  state: (): LocalesStore => ({
    locales: 'zh_CN'
  }),
  persist: true,
  getters: {
    getLocales(): Locales {
      return this.locales
    }
  },
  actions: {
    setLocales(lang: Locales): void {
      this.locales = lang
    }
  }
})
