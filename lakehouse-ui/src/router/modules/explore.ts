/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

export default {
  path: '/explore',
  name: 'explore',
  meta: {
    title: '数据探索'
  },
  redirect: { name: 'explore-sqlconsole' },
  component: () => import('@/components/headnav'),
  children: [
    {
      path: '/explore/sqlconsole',
      name: 'explore-sqlconsole',
      components: {
        'sql-console-container': () => import('@/views/explore/sqlconsole')
      },
      meta: {
        title: 'Sql Console',
        auth: []
      },
      children: [
        {
          path: '/explore/sqlconsole/:dbId',
          name: 'explore-sqlconsole-tab',
          components: {
            'sql-console-tab': () =>
              import('@/views/explore/sqlconsole/sqlquery')
          },
          meta: {
            title: 'Sql Console Query Tab',
            auth: []
          }
        }
      ]
    }
  ]
}
