/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

export default {
  path: '/instance',
  name: 'instance',
  meta: { title: 'data-quality' },
  redirect: { name: 'instance-list' },
  component: () => import('@/layouts/content'),
  children: [
    {
      path: '/instance',
      name: 'instance-list',
      component: () => import('@/views/instance/list'),
      meta: {
        title: '',
        auth: []
      }
    },
    {
      path: '/instance/details/:instanceId',
      name: 'instance-details',
      component: () => import('@/views/instance/details'),
      meta: {
        title: '',
        auth: []
      }
    }
  ]
}
