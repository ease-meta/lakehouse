/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { parseISO } from 'date-fns'
import _ from 'lodash'

/**
 * A simple uuid generator, support prefix and template pattern.
 *
 * @example
 *
 *  uuid('v-') // -> v-xxx
 *  uuid('v-ani-%{s}-translate')  // -> v-ani-xxx
 */
export function uuid(prefix: string) {
  const id = Math.floor(Math.random() * 10000).toString(36)
  return prefix
    ? ~prefix.indexOf('%{s}')
      ? prefix.replace(/%\{s\}/g, id)
      : prefix + id
    : id
}

export const parseTime = (dateTime: string | number) => {
  if (_.isString(dateTime) === true) {
    return parseISO(dateTime as string)
  } else {
    return new Date(dateTime)
  }
}
