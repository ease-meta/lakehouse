/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { SearchOutlined } from '@vicons/antd'
import { Info24Filled } from '@vicons/fluent'
import { useDebounce } from '@vueuse/core'
import { NIcon, NInput } from 'naive-ui'
import { computed, defineComponent, PropType, ref } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'

const props = {
  jobId: {
    type: String,
    default: ''
  },
  columnList: {
    type: Array as PropType<string[]>,
    default: []
  },
  resultSet: {
    type: Object,
    default: {}
  },
  resultString: {
    type: String,
    default: ''
  },
  bottomDivHeight: {
    type: Number
  }
}

const SuccessResult = defineComponent({
  name: 'success-result',
  props,
  setup(props) {
    const { t } = useI18n()

    const searchText = ref<string>('')
    const debounced = useDebounce(searchText, 500)
    const columns = computed(() => {
      const list: any[] = []
      props.columnList?.forEach((column: string) => {
        list.push({
          title: column,
          minWidth: 150,
          key: column
        })
      })
      return list
    })

    const data = computed(() => {
      const list: any[] = []
      const search = debounced.value
      if (Array.isArray(props.resultSet)) {
        if (props.resultSet?.length !== 0) {
          props.resultSet?.forEach((result) => {
            let obj = {}
            let match = false
            props.columnList.forEach((item, index) => {
              obj = {
                ...obj,
                [item]: result[index] === null ? 'null' : result[index]
              }
              if (
                result[index] &&
                result[index].toString().toLowerCase().includes(search)
              ) {
                match = true
              }
            })
            match && list.push(obj)
          })
        }
      }
      return list
    })

    return {
      t,
      searchText,
      columns,
      data
    }
  },
  render() {
    return (
      <div class={styles.successExec}>
        {this.resultString ? (
          <p style='font-size: 12px; white-space: pre-line'>
            {this.resultString}
          </p>
        ) : this.resultSet === null ? (
          <div class={styles.nullmsgExec}>
            {this.t('explore.success_result_run_success')}
          </div>
        ) : (
          <div>
            <div class='header'>
              <div class='search-bar'>
                <div style='width: 320px; margin-top: 8px'>
                  <Info24Filled />
                  {this.t('explore.success_result_search_limit')}
                </div>
                <NInput
                  v-model={[this.searchText, 'value']}
                  placeholder={this.t('explore.success_result_search_tip')}
                >
                  {{
                    prefix: () => (
                      <NIcon>
                        <SearchOutlined />
                      </NIcon>
                    )
                  }}
                </NInput>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
})

export default SuccessResult
