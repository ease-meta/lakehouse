/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { getInstanceList } from '@/service/modules/instance'
import { ref, watch } from 'vue'

export default function useEngineConfig() {
  const engineType = ref<string>('Spark')

  // TODO: need add Flink & Presto engine
  const engineTypeSet = [
    {
      label: 'Spark',
      value: 'Spark'
    }
  ]
  const instanceOptions = ref<any>([])
  const instance = ref<string>('')

  watch(
    () => engineType.value,
    () => {
      getInstanceList(engineType.value).then((res: string[]) => {
        res?.map((instance: string) => {
          instanceOptions.value.push({
            label: instance,
            value: instance
          })
        })
      })
    },
    {
      immediate: true
    }
  )

  return {
    engineTypeSet,
    engineType,
    instanceOptions,
    instance
  }
}
