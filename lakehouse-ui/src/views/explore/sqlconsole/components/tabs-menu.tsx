/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useTabsRouteStore } from '@/store/sqlconsole/tabs-route'
import { TabRouteConfig } from '@/store/sqlconsole/types'
import { NTabPane, NTabs } from 'naive-ui'
import { defineComponent, ref, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router'

const TabsMenu = defineComponent({
  name: 'tabs-menu',
  setup() {
    const dbIds = ref<string[]>([])
    const activeTab = ref<string>('')
    const tabkey = ref<number>(0)

    const tagsList = ref<any>([])
    const showTags = ref<boolean>(true)

    const $route = useRoute()
    const $router = useRouter()
    const $tabsRouteStore = useTabsRouteStore()

    const changeRouteNow = () => {
      tabkey.value = tabkey.value + 1
    }

    watch(
      () => $route.params.dbId,
      () => {
        changeRouteNow()
        dbIds.value = $tabsRouteStore.getDbIds || []
        activeTab.value = $route.params.dbId as string
      },
      {
        immediate: true
      }
    )

    const isActive = (path: string) => {
      return path === $route.fullPath
    }

    const handleRemoveSqlQuery = (name: string) => {
      const deletePath = '/explore/sqlconsole/' + name
      const allRoutes = $tabsRouteStore.getAllRoutes as any[]
      const deleteRouteIndex = allRoutes.findIndex((item: TabRouteConfig) => {
        return item.path === $route.path
      })
      // if removed tab's router equals with current router, then tab move to prev tab
      if ($route.path === deletePath) {
        $router.push(allRoutes[deleteRouteIndex - 1].path)
      }
      // remove closed tab's router
      $tabsRouteStore.deleteRoute(name)
      dbIds.value = dbIds.value.filter((value) => value !== name)
    }

    const handleSwitch = (name: string) => {
      $router.push('/explore/sqlconsole/' + name)
    }

    return {
      activeTab,
      dbIds,
      handleRemoveSqlQuery,
      handleSwitch,
      changeRouteNow,
      tagsList,
      showTags,
      isActive,
      tabkey
    }
  },
  render() {
    return this.showTags ? (
      <NTabs
        type='card'
        v-model={[this.activeTab, 'value']}
        closable={true}
        onClose={this.handleRemoveSqlQuery}
        onUpdateValue={this.handleSwitch}
      >
        {this.dbIds.map((item, index) => {
          return (
            <NTabPane
              tab={item}
              name={item}
              key={item}
              closable={index != 0}
            ></NTabPane>
          )
        })}
      </NTabs>
    ) : null
  }
})

export default TabsMenu
