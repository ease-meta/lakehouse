/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { BugFilled } from '@vicons/antd'
import {
  AlertOn20Filled,
  CheckmarkCircle16Filled,
  Clock12Regular,
  TextBulletListSquare24Regular
} from '@vicons/fluent'
import { NIcon } from 'naive-ui'
import { h, VNode } from 'vue'
import { useI18n } from 'vue-i18n'

export default function useStatus() {
  const { t } = useI18n()

  const switchStatus = (status: string) => {
    let cnStatus = ''
    switch (status) {
      case 'ACCEPTED':
        cnStatus = t('explore.accepted_status')
        break
      case 'SUBMITTED':
        cnStatus = t('explore.submitted_status')
        break
      case 'RUNNING':
        cnStatus = t('explore.running_status')
        break
      case 'SUBMIT_FAILED':
        cnStatus = t('explore.submit_failed_status')
        break
      case 'CANCELLED':
        cnStatus = t('explore.cancelled_status')
        break
      case 'FAILED':
        cnStatus = t('explore.failed_status')
        break
      case 'FINISHED':
        cnStatus = t('explore.finished_status')
        break
      case 'LOST':
        cnStatus = t('explore.lost_status')
        break
      case 'UNKNOWN':
        cnStatus = t('explore.unknown_status')
        break
    }
    return cnStatus
  }

  const switchIcon = (status: string): VNode => {
    let icon = {} as VNode
    switch (status) {
      case 'RUNNING':
      case 'SUBMITTED':
      case 'ACCEPTED':
        icon = h(NIcon, {}, { default: () => h(Clock12Regular) })
        break
      case 'FINISHED':
        icon = h(
          NIcon,
          { color: '#10C038' },
          { default: () => h(CheckmarkCircle16Filled) }
        )
        break
      case 'LOST':
      case 'SUBMIT_FAILED':
      case 'CANCELLED':
        icon = h(NIcon, {}, { default: () => h(AlertOn20Filled) })
        break
      case 'FAILED':
      case 'UNKNOWN':
        icon = h(NIcon, { color: 'red' }, { default: () => h(BugFilled) })
        break
      default:
        icon = h(NIcon, {}, { default: () => h(TextBulletListSquare24Regular) })
        break
    }
    return icon
  }

  return {
    switchStatus,
    switchIcon
  }
}
