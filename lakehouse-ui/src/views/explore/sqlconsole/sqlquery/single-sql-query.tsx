/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { SqlQueryTab } from '@/service/modules/sqlconsole/types'
import { NTabPane, NTabs } from 'naive-ui'
import { defineComponent } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import QueryPanel from './query-panel'
import { useSingleSqlQuery } from './use-single-sql-query'

const props = {
  dbName: {
    type: String,
    default: ''
  }
}

const SingleSqlQuery = defineComponent({
  name: 'single-sql-query',
  props,
  setup(props) {
    const { t } = useI18n()

    const {
      tabs,
      currentSqlTab,
      sqlTabs,
      onBeforeRemoveTab,
      onAddSqlTab,
      onChangeConsoleBottomHeight
    } = useSingleSqlQuery(props.dbName)

    return {
      t,
      tabs,
      currentSqlTab,
      sqlTabs,
      onBeforeRemoveTab,
      onAddSqlTab,
      onChangeConsoleBottomHeight
    }
  },
  render() {
    return (
      <div class={styles.singleSqlQuery}>
        <div class={styles.tabList}>
          <NTabs
            type='card'
            animated
            v-model={[this.currentSqlTab, 'value']}
            addable={true}
            onClose={this.onBeforeRemoveTab}
            onAdd={this.onAddSqlTab}
          >
            {this.sqlTabs.map((tab: SqlQueryTab, index: number) => {
              return (
                <NTabPane
                  key={tab.name}
                  name={tab.name}
                  tab={tab.label}
                  displayDirective='show'
                  closable={tab.closable}
                >
                  <QueryPanel
                    consoleName={tab.name}
                    dbName={tab.instance}
                    bottomDivHeight={tab.bottomDivHeight}
                    onChangeCurrentSqlTabBottomDiv={
                      this.onChangeConsoleBottomHeight
                    }
                    order={index}
                  ></QueryPanel>
                </NTabPane>
              )
            })}
          </NTabs>
        </div>
      </div>
    )
  }
})

export default SingleSqlQuery
