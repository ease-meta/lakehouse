/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, onMounted, ref, watch } from 'vue'
import _CodeMirror from 'codemirror'
import 'codemirror/lib/codemirror.css'
import 'codemirror/mode/sql/sql'
import 'codemirror/addon/hint/show-hint.css'
import 'codemirror/addon/hint/show-hint.js'
import 'codemirror/addon/hint/sql-hint'
import 'codemirror/theme/bespin.css'
// import { useRoute } from 'vue-router'
// import { useSqlExecTabStore } from '@/store/sqlconsole/sql-exec-tab'

const props = {
  sql: {
    type: String,
    default: ''
  },
  onChangeText: {
    type: Function,
    default: () => {}
  },
  onSelectText: {
    type: Function,
    default: () => {}
  }
}

const CodeMirror = window.CodeMirror || _CodeMirror

const CodeEditor = defineComponent({
  name: 'code-editor',
  props,
  setup(props: any) {
    // const $route = useRoute()
    // const $sqlExecTabStore = useSqlExecTabStore()

    const code = ref<any>('')
    let CodeMirrorEditor = {} as any
    // const dbName = computed(() => $route.params.dbId)

    onMounted(() => {
      CodeMirrorEditor = CodeMirror.fromTextArea(code.value, {
        mode: 'sql', // supported llanguage
        lineNumbers: true, // show line code
        lineWrapping: true,
        lineSeparator: '<br/>',
        // hintOptions: {
        //   completeSingle: false
        // },
        theme: 'bespin',
        lineWiseCopyCut: false
      })
      CodeMirrorEditor.refresh()
      CodeMirrorEditor.on('change', function (instance: any, change: any) {
        // It is used to judge whether the user enters blank or line feed
        CodeMirrorEditor.curChangeValue = change.text[0]
        // change event handler to call onChangeText function
        props.onChangeText(CodeMirrorEditor.getValue())
      })
      // replication behavior
      CodeMirrorEditor.on('copy', function (instance: any, event: any) {
        // eslint-disable-next-line no-console
        console.log(event.target.value.replace(/<br\/>/g, '\n'))
        // context.root
        // .$copyText(event.target.value.replace(/<br\/>/g, '\n'))
        // .then((text: any) => {
        //   // eslint-disable-next-line no-console
        //   console.log(text)
        // })
      })

      // CodeMirrorEditor.value.on('cursorActivity', function () {
      //   // self.curChangeValue use to control hint option
      //   if (
      //     CodeMirrorEditor.value.curChangeValue &&
      //     CodeMirrorEditor.value.curChangeValue.length < 2 &&
      //     /\w|\./g.test(CodeMirrorEditor.value.curChangeValue)
      //   ) {
      //     CodeMirrorEditor.value.showHint()
      //   }
      //   props.onSelectText(CodeMirrorEditor.value.getSelection())
      // })

      // const _replace = props.sql.replace('\n', '<br/>')
      // CodeMirrorEditor.value.setValue(_replace)
      // handleShowHint()
    })

    // const handleShowHint = () => {
    //   const tablesAll = $sqlExecTabStore.getAllExecTabs
    //   const tables = tablesAll.filter(
    //     (item) => item.label === (dbName.value as string)
    //   )

    //   CodeMirrorEditor.value.setOption('hintOptions', {
    //     ...CodeMirrorEditor.value.getOption('hintOptions'),
    //     tables
    //   })
    // }

    watch(
      () => props.sql,
      () => {
        const _replace = props.sql.replace('\n', '<br/>')
        CodeMirrorEditor.setValue(_replace)
      },
      {
        immediate: false
      }
    )

    return {
      code
    }
  },
  render() {
    return <textarea ref='code'></textarea>
  }
})

export default CodeEditor
