/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useTabsRouteStore } from '@/store/sqlconsole/tabs-route'
import { computed, defineComponent, ref, watch } from 'vue'
import { useRoute } from 'vue-router'
import styles from './index.module.scss'
import SingleSqlQuery from './single-sql-query'

const SqlConsole = defineComponent({
  name: 'SqlConsole',
  setup() {
    const $route = useRoute()
    const $tabsRouteStore = useTabsRouteStore()

    const dbIds = ref<string[]>([])
    const dbIdParam = computed(() => $route.params.dbId)

    watch(
      () => $route.params.dbId,
      () => {
        dbIds.value = $tabsRouteStore.getDbIds || []
      },
      {
        immediate: true
      }
    )

    return {
      dbIds,
      dbIdParam
    }
  },
  render() {
    return (
      <div class={styles.sqlQueryClone}>
        {this.dbIds.map((dbName: string) => {
          return (
            <div>
              <SingleSqlQuery
                key={dbName}
                dbName={dbName}
                v-show={this.$route.params.dbId === dbName}
              ></SingleSqlQuery>
            </div>
          )
        })}
      </div>
    )
  }
})

export default SqlConsole
