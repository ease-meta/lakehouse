/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent } from 'vue'
import { NButton, NSpace } from 'naive-ui'
import style from '../index.module.scss'
import IconCreateInstance from '@/assets/images/profile/icon_create_instance.svg'
import IconCreateDatabase from '@/assets/images/profile/icon_create_database.svg'
import IconDataSynchronization from '@/assets/images/profile/icon_data_synchronization.svg'
import IconDataDiscovery from '@/assets/images/profile/icon_data_discovery.png'
import IconDataDirectory from '@/assets/images/profile/icon_data_directory.svg'
import IconSqlConsole from '@/assets/images/profile/icon_sql_console.svg'
import { useI18n } from 'vue-i18n'
import { useRouter, Router } from 'vue-router'

const Guide = defineComponent({
  name: 'Guide',
  setup() {
    const router: Router = useRouter()
    const { t } = useI18n()
    const onHandleToInstance = () => {
      router.push('/instance')
    }
    return {
      IconCreateInstance,
      IconCreateDatabase,
      IconDataSynchronization,
      IconDataDiscovery,
      IconDataDirectory,
      IconSqlConsole,
      t,
      onHandleToInstance
    }
  },
  render() {
    const { t, onHandleToInstance } = this
    return (
      <div class={style.guide}>
        <span class={style.title}>{t('home.use_guide')}</span>
        <div class={style['guide-box']}>
          <div class={style['serial']}>
            <div class={style['serial-number']}>1</div>
            <span class={style['serial-title']}>
              {t('home.initialize_instance')}
            </span>
          </div>
          <NSpace style='margin-top: 5px' wrap={false}>
            <div class={style['guide-line']}></div>
            <div class={style['guide-introduce']}>
              <NSpace align='center' justify='space-between'>
                <NSpace align='center' style='height: 80px'>
                  <img
                    class={style['introduce-icon']}
                    src={IconCreateInstance}
                  />
                  <NSpace vertical>
                    <span class={style['introduce-titile']}>
                      {t('home.create_instance')}
                    </span>
                    <span class={style['introduce-content']}>
                      {t('home.create_instance_desc')}
                    </span>
                  </NSpace>
                </NSpace>
                <NButton
                  onClick={onHandleToInstance}
                  class={style['introduce-link']}
                  text
                >
                  {t('home.create_now')}
                </NButton>
              </NSpace>
            </div>
          </NSpace>
          <div class={style['serial']}>
            <div class={style['serial-number']}>2</div>
            <span class={style['serial-title']}>
              {t('home.bulid_datalake')}
            </span>
          </div>
          <NSpace style='margin-top: 5px' wrap={false}>
            <div class={style['guide-line']}></div>
            <div>
              <div class={style['guide-introduce']}>
                <NSpace align='center' justify='space-between'>
                  <NSpace align='center' style='height: 80px'>
                    <img
                      class={style['introduce-icon']}
                      src={this.IconCreateDatabase}
                    />
                    <NSpace vertical>
                      <span class={style['introduce-titile']}>
                        {t('home.create_database')}
                      </span>
                      <span class={style['introduce-content']}>
                        {t('home.create_database_desc')}
                      </span>
                    </NSpace>
                  </NSpace>
                  <NButton class={style['introduce-link']} text>
                    {t('home.create_now')}
                  </NButton>
                </NSpace>
              </div>
              <NSpace>
                <div class={style['guide-introduce']}>
                  <NSpace align='center' justify='space-between'>
                    <NSpace align='center' style='height: 80px'>
                      <img
                        class={style['introduce-icon']}
                        src={this.IconDataSynchronization}
                      />
                      <NSpace vertical>
                        <span class={style['introduce-titile']}>
                          {t('home.data_sync')}
                        </span>
                        <span class={style['introduce-content']}>
                          {t('home.data_sync_desc')}
                        </span>
                      </NSpace>
                    </NSpace>
                    <NButton class={style['introduce-link']} text>
                      {t('home.one_click_sync')}
                    </NButton>
                  </NSpace>
                </div>
                <div class={style['guide-introduce']}>
                  <NSpace align='center' justify='space-between'>
                    <NSpace align='center' style='height: 80px'>
                      <img
                        class={style['introduce-icon']}
                        src={this.IconDataDiscovery}
                      />
                      <NSpace vertical>
                        <span class={style['introduce-titile']}>
                          {t('home.metadata_discovery')}
                        </span>
                        <span class={style['introduce-content']}>
                          {t('home.metadata_discovery_desc')}
                        </span>
                      </NSpace>
                    </NSpace>
                    <NButton class={style['introduce-link']} text>
                      {t('home.discovry_now')}
                    </NButton>
                  </NSpace>
                </div>
              </NSpace>
              <div class={style['guide-introduce']}>
                <NSpace align='center' justify='space-between'>
                  <NSpace align='center' style='height: 80px'>
                    <img
                      class={style['introduce-icon']}
                      src={this.IconDataDirectory}
                    />
                    <NSpace vertical>
                      <span class={style['introduce-titile']}>
                        {t('home.data_directory')}
                      </span>
                      <span class={style['introduce-content']}>
                        {t('home.data_directory_desc')}
                      </span>
                    </NSpace>
                  </NSpace>
                  <NButton class={style['introduce-link']} text>
                    {t('home.view_data_directory')}
                  </NButton>
                </NSpace>
              </div>
            </div>
          </NSpace>
          <div class={style['serial']}>
            <div class={style['serial-number']}>3</div>
            <span class={style['serial-title']}>{t('home.data_explore')}</span>
          </div>
          <NSpace style='margin: 5px 0 0 22px' wrap={false}>
            <div class={style['guide-introduce']}>
              <NSpace align='center' justify='space-between'>
                <NSpace align='center' style='height: 80px'>
                  <img
                    class={style['introduce-icon']}
                    src={this.IconSqlConsole}
                  />
                  <NSpace vertical>
                    <span class={style['introduce-titile']}>
                      {t('home.sql_console')}
                    </span>
                    <span class={style['introduce-content']}>
                      {t('home.sql_console_desc')}
                    </span>
                  </NSpace>
                </NSpace>
                <NButton class={style['introduce-link']} text>
                  {t('home.create_now')}
                </NButton>
              </NSpace>
            </div>
          </NSpace>
        </div>
      </div>
    )
  }
})

export default Guide
