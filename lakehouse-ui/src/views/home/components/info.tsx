/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, onMounted, reactive, ref } from 'vue'
import style from '../index.module.scss'
import IconInstanceNum from '@/assets/images/profile/icon_instance_num.svg'
import IconWorkNum from '@/assets/images/profile/icon_work_num.svg'
import { useI18n } from 'vue-i18n'
import { getInstanceList } from '@/service/modules/instance'
import { useRouter, Router } from 'vue-router'
import { NButton } from 'naive-ui'

const Info = defineComponent({
  name: 'Info',
  setup() {
    const router: Router = useRouter()
    const { t } = useI18n()
    const data = reactive({
      instanceNum: ref(0),
      jobNum: ref(0)
    })

    const onHandleToInstance = () => {
      router.push('/instance')
    }

    onMounted(async () => {
      const res = await getInstanceList('Spark')
      data.instanceNum = res.length
    })

    return {
      IconInstanceNum,
      IconWorkNum,
      t,
      data,
      onHandleToInstance
    }
  },
  render() {
    const { t, data, onHandleToInstance } = this
    return (
      <div class={style['content-header']}>
        <div class={style['panel-box']}>
          <div class={style.panel}>
            <img class={style.img} src={this.IconInstanceNum} />
            <div class={style.info}>{t('home.instance_number')}</div>
            <div class={style['instance-number']}>{data.instanceNum}</div>
            <div class={style['instance-unit']}>{t('home.sets')}</div>
          </div>
          <NButton onClick={onHandleToInstance} text class={style.link}>
            {t('home.view_instance_list')}
          </NButton>
        </div>
        <div class={style['panel-box']}>
          <div class={style.panel}>
            <img class={style.img} src={this.IconWorkNum} />
            <div class={style.info}>{t('home.today_job_number')}</div>
            <div class={style['instance-number']}>{data.jobNum}</div>
            <div class={style['instance-unit']}>{t('home.sets')}</div>
          </div>
          <NButton text class={style.link}>
            {t('home.view_job_monitor')}
          </NButton>
        </div>
      </div>
    )
  }
})

export default Info
