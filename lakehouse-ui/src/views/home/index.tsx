/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, ref } from 'vue'
import { startOfToday, getTime } from 'date-fns'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import Info from './components/info'
import Guide from './components/guide'

export default defineComponent({
  name: 'home',
  setup() {
    const { t } = useI18n()
    const dateRef = ref([getTime(startOfToday()), Date.now()])

    return {
      t,
      dateRef
    }
  },
  render() {
    return (
      <div class={styles.container}>
        <Info></Info>
        <Guide></Guide>
      </div>
    )
  }
})
