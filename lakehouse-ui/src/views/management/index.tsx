/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import {
  NButton,
  NCard,
  NDataTable,
  NDatePicker,
  NForm,
  NFormItem,
  NInput,
  NPagination,
  NSpace
} from 'naive-ui'
import { defineComponent, onMounted, toRefs, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import { useTable } from './use-table'

const list = defineComponent({
  name: 'list',
  setup() {
    const { t } = useI18n()
    const { variables, createColumns } = useTable()

    const clickCreate = () => {
      variables.showModalRef = true
    }

    onMounted(() => {
      createColumns(variables)
    })

    watch(useI18n().locale, () => {
      createColumns(variables)
    })

    return {
      t,
      clickCreate,
      ...toRefs(variables)
    }
  },
  render() {
    const { t, loadingRef } = this
    return (
      <div class={styles.container}>
        <div class={styles['table-container']}>
          <NCard>
            <NForm labelPlacement='left'>
              <NSpace class={styles['search-header']} justify='space-between'>
                <NFormItem
                  class={styles['search-input']}
                  label={t('synchronization.job_name')}
                >
                  <NInput></NInput>
                </NFormItem>
                <NFormItem
                  class={styles['search-input']}
                  label={t('synchronization.job_id')}
                >
                  <NInput></NInput>
                </NFormItem>
                <NFormItem
                  class={styles['search-input']}
                  label={t('synchronization.schedule_status')}
                >
                  <NDatePicker type='datetimerange'></NDatePicker>
                </NFormItem>
                <NFormItem
                  class={styles['search-input']}
                  label={t('synchronization.create_user')}
                >
                  <NInput></NInput>
                </NFormItem>
                <NFormItem
                  class={styles['search-input']}
                  label={t('synchronization.job_type')}
                >
                  <NInput></NInput>
                </NFormItem>
                <NSpace class={styles['search-input']}>
                  <NButton type='primary'>{t('common.search')}</NButton>
                  <NButton>{t('common.reset')}</NButton>
                </NSpace>
              </NSpace>
            </NForm>
          </NCard>
          <NSpace class={styles['table-header']} justify='space-between'>
            <div class={styles['header-buttons']}>
              <NButton type='primary' style='margin-left: 10px'>
                {t('common.create')}
              </NButton>
              <NButton style='margin-left: 10px'>{t('common.delete')}</NButton>
              <NButton style='margin-left: 10px'>{t('common.refresh')}</NButton>
            </div>
          </NSpace>
          <div class={styles['table-list']}>
            <NDataTable
              loading={loadingRef}
              columns={this.columns}
              data={this.tableData}
              scrollX={this.tableWidth}
            />
          </div>
          <div class={styles.pagination}>
            <NPagination
              v-model:page={this.page}
              v-model:page-size={this.pageSize}
              pageCount={this.totalPage}
              showSizePicker
              pageSizes={[10, 30, 50]}
              showQuickJumper
            />
          </div>
        </div>
      </div>
    )
  }
})

export default list
