/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { h, reactive, ref } from 'vue'
import { useI18n } from 'vue-i18n'
import { NButton } from 'naive-ui'
import { runJob, deleteJob } from '@/service/modules/synchronization'

export function useTable() {
  const { t } = useI18n()

  const variables = reactive({
    columns: [],
    tableWidth: 500,
    tableData: [],
    page: ref(1),
    pageSize: ref(10),
    searchVal: ref(null),
    totalPage: ref(1),
    showModalRef: ref(false),
    statusRef: ref(0),
    row: {},
    loadingRef: ref(false)
  })

  const onHandleExcute = (row: any) => {
    runJob(row.id)
  }

  const onHandleDelete = (row: any) => {
    deleteJob(row.id)
  }

  const createColumns = (variables: any) => {
    variables.columns = [
      {
        type: 'selection'
      },
      {
        title: t('directory.table_name'),
        key: 'jobId'
      },
      {
        title: t('directory.database_name'),
        key: 'filename'
      },
      {
        title: t('directory.location'),
        key: 'engine'
      },
      {
        title: '',
        key: 'status'
      },
      {
        title: t('directory.ref_task_type'),
        key: 'refTaskType'
      },
      {
        title: t('directory.ref_task_id'),
        key: 'refTaskId'
      },
      {
        title: t('directory.creator'),
        key: 'creator'
      },
      {
        title: t('directory.create_time'),
        key: 'createTime'
      },
      {
        title: t('directory.last_modifier'),
        key: 'lastModifier'
      },
      {
        title: t('directory.last_modifier_time'),
        key: 'lastModifierTime'
      },
      {
        title: t('common.action'),
        key: 'actions',
        render(row: any) {
          return h('span', null, [
            h(
              NButton,
              {
                text: true,
                type: 'info',
                strong: true,
                size: 'small',
                onClick: () => {
                  onHandleExcute(row)
                }
              },
              { default: () => t('directory.search_data') }
            ),
            h(
              NButton,
              {
                text: true,
                type: 'info',
                strong: true,
                size: 'small',
                style: {
                  'margin-left': '10px'
                },
                onClick: () => {
                  onHandleDelete(row)
                }
              },
              { default: () => t('common.delete') }
            )
          ])
        }
      }
    ]
  }

  return {
    variables,
    createColumns
  }
}
