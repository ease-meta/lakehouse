/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import {
  NButton,
  NDataTable,
  NIcon,
  NInput,
  NPagination,
  NSpace
} from 'naive-ui'
import { defineComponent, onMounted, toRefs, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import { useTable } from './use-table'
import { SearchOutlined } from '@vicons/antd'
import AddModal from './components/add-modal'
import ResetForm from './components/reset-modal'

const list = defineComponent({
  name: 'list',
  setup() {
    const { t } = useI18n()
    const {
      variables,
      createColumns,
      getTableData,
      handleCheck,
      handleBatchDelete
    } = useTable()

    const clickAdd = () => {
      variables.showAddModalRef = true
    }

    const clickDelete = () => {
      handleBatchDelete()
    }

    const onCancelModal = () => {
      variables.showAddModalRef = false
      variables.showResetModalRef = false
    }

    const onConfirmModal = () => {
      variables.showAddModalRef = false
      variables.showResetModalRef = false
      getTableData()
    }

    const onUpdatePageSize = () => {
      variables.pageNo = 1
      getTableData()
    }

    const onSearch = () => {
      variables.pageNo = 1
      getTableData()
    }

    onMounted(() => {
      createColumns(variables)
      getTableData()
    })

    watch(useI18n().locale, () => {
      createColumns(variables)
    })

    return {
      t,
      ...toRefs(variables),
      clickAdd,
      clickDelete,
      onCancelModal,
      onConfirmModal,
      handleCheck,
      getTableData,
      onUpdatePageSize,
      onSearch
    }
  },
  render() {
    const {
      t,
      clickAdd,
      clickDelete,
      handleCheck,
      onCancelModal,
      onConfirmModal,
      getTableData,
      onUpdatePageSize,
      onSearch
    } = this
    return (
      <div class={styles.container}>
        <div class={styles['table-container']}>
          <NSpace class={styles['table-header']} justify='space-between'>
            <div class={styles['header-buttons']}>
              <NButton type='info' onClick={clickAdd}>
                {t('user.addUser')}
              </NButton>
              <NButton
                style='margin-left: 10px'
                disabled={this.checkedRowKeys.length <= 0}
                onClick={clickDelete}
              >
                {t('common.delete')}
              </NButton>
            </div>
            <div class={styles['header-search']}>
              <NInput v-model={[this.searchVal, 'value']} onInput={onSearch}>
                {{
                  prefix: () => (
                    <NIcon>
                      <SearchOutlined />
                    </NIcon>
                  )
                }}
              </NInput>
            </div>
          </NSpace>
          <div class={styles['table-list']}>
            <NDataTable
              loading={this.loadingRef}
              columns={this.columns}
              data={this.tableData}
              scrollX={this.tableWidth}
              rowKey={(row) => row.id}
              onUpdate:checkedRowKeys={handleCheck}
            />
          </div>
          <div class={styles.pagination}>
            <NSpace justify='space-between'>
              <span>
                {t('pagination.total')} {this.totalElements}{' '}
                {t('pagination.record')}
              </span>
              <NPagination
                v-model:page={this.pageNo}
                v-model:page-size={this.pageSize}
                pageCount={this.totalPage}
                showSizePicker
                pageSizes={[10, 20, 30, 50]}
                showQuickJumper
                onUpdatePage={getTableData}
                onUpdatePageSize={onUpdatePageSize}
              />
            </NSpace>
          </div>
        </div>
        <AddModal
          showModalRef={this.showAddModalRef}
          onCancelModal={onCancelModal}
          onConfirmModal={onConfirmModal}
        ></AddModal>
        <ResetForm
          row={this.row}
          showModalRef={this.showResetModalRef}
          onCancelModal={onCancelModal}
          onConfirmModal={onConfirmModal}
        ></ResetForm>
      </div>
    )
  }
})

export default list
