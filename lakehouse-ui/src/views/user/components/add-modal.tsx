/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType, toRefs } from 'vue'
import { NModal, NForm, NFormItem, NInput, NCard, NButton } from 'naive-ui'
import { useForm } from './use-form'
import style from '../index.module.scss'
import { createUser } from '@/service/modules/user'

const props = {
  showModalRef: {
    type: Boolean as PropType<boolean>,
    default: false
  }
}

const AddModal = defineComponent({
  name: 'AddModal',
  props,
  emits: ['cancelModal', 'confirmModal'],
  setup(props, ctx) {
    const { variables, t, resetForm } = useForm()

    const cancelModal = () => {
      ctx.emit('cancelModal', props.showModalRef)
      resetForm()
    }

    const confirmModal = async () => {
      if (variables.saving) return
      variables.saving = true
      try {
        await createUser(
          variables.formItem.userName,
          variables.formItem.userPassword,
          variables.formItem.description
        )
        resetForm()
        variables.saving = false
        ctx.emit('confirmModal', props.showModalRef)
      } catch (err) {
        variables.saving = false
      }
    }

    return { ...toRefs(variables), t, cancelModal, confirmModal }
  },
  render() {
    const { t, cancelModal, confirmModal, formItem, rules } = this
    return (
      <NModal show={this.showModalRef} onMaskClick={cancelModal}>
        <NCard
          title='新增用户'
          class={style['add-modal-card']}
          closable
          onClose={cancelModal}
        >
          {{
            default: () => (
              <NForm
                labelPlacement='left'
                labelWidth='auto'
                size='medium'
                rules={rules}
                ref='addUserFormRef'
              >
                <NFormItem label={t('user.addModal.username')} path='userName'>
                  <NInput v-model:value={formItem.userName}></NInput>
                </NFormItem>
                <NFormItem
                  label={t('user.addModal.password')}
                  path='userPassword'
                >
                  <NInput
                    v-model:value={formItem.userPassword}
                    type='password'
                  ></NInput>
                </NFormItem>
                <NFormItem
                  label={t('user.addModal.passwordConfirm')}
                  path='userPasswordConfirm'
                >
                  <NInput
                    v-model:value={formItem.userPasswordConfirm}
                    type='password'
                  ></NInput>
                </NFormItem>
                <NFormItem label={t('user.addModal.description')}>
                  <NInput v-model:value={formItem.description}></NInput>
                </NFormItem>
              </NForm>
            ),
            footer: () => (
              <div>
                <NButton onClick={cancelModal}>
                  {t('user.addModal.cancel')}
                </NButton>
                <NButton onClick={confirmModal}>
                  {t('user.addModal.submit')}
                </NButton>
              </div>
            )
          }}
        </NCard>
      </NModal>
    )
  }
})

export default AddModal
