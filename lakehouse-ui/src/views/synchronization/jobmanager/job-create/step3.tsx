/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType, toRefs } from 'vue'
import {
  NButton,
  NCard,
  NForm,
  NFormItem,
  NInput,
  NRadioButton,
  NRadioGroup,
  NSelect,
  NSpace,
  NIcon
} from 'naive-ui'
import { useForm } from './use-form'
import { ReloadOutlined } from '@vicons/antd'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const Step3 = defineComponent({
  name: 'Step3',
  props,
  setup() {
    const { t, variables, CONNECTOR_GROUP_LIST, CONNECTOR_TYPE_LIST } =
      useForm()
    return {
      t,
      ...toRefs(variables),
      CONNECTOR_GROUP_LIST,
      CONNECTOR_TYPE_LIST
    }
  },
  render() {
    const { t } = this
    return (
      <NSpace>
        <NForm labelPlacement='left' labelWidth='auto'>
          <NCard
            style='width: 50vw'
            title={t('synchronization.create_form.data_source_config')}
          >
            <NFormItem label={t('synchronization.create_form.connector_group')}>
              <NSelect
                v-model:value={this.createForm.connectorGroup}
                options={this.CONNECTOR_GROUP_LIST}
              ></NSelect>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.connector_type')}>
              <NSelect
                v-model:value={this.createForm.connenctorType}
                disabled={!this.createForm.connectorGroup}
                options={this.CONNECTOR_TYPE_LIST}
              ></NSelect>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.connector_id')}>
              <NInput></NInput>
              <NButton style='margin-left: 12px'>
                <NIcon>
                  <ReloadOutlined></ReloadOutlined>
                </NIcon>
              </NButton>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.database_name')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.table')}>
              <NRadioGroup>
                <NRadioButton
                  label={t('synchronization.create_form.all')}
                ></NRadioButton>
                <NRadioButton
                  label={t('synchronization.create_form.multi')}
                ></NRadioButton>
              </NRadioGroup>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.table_name')}>
              <NInput></NInput>
            </NFormItem>
          </NCard>
          <NCard title={t('synchronization.create_form.data_target_config')}>
            <NFormItem label={t('synchronization.create_form.database_name')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.table_prefix')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.store_type')}>
              <NRadioGroup>
                <NRadioButton label='对象存储'></NRadioButton>
              </NRadioGroup>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.connector_path')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.ak')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.sk')}>
              <NInput></NInput>
              <NButton style='margin-left: 12px'></NButton>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.store_path')}>
              <NInput></NInput>
            </NFormItem>
            <NFormItem label={t('synchronization.create_form.partition')}>
              <NInput></NInput>
            </NFormItem>
          </NCard>
        </NForm>
      </NSpace>
    )
  }
})

export default Step3
