/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, ref, PropType } from 'vue'
import { NButton, NSpace, NStep, NSteps } from 'naive-ui'
import styles from '../index.module.scss'
import { useForm } from './use-form'
import Step1 from './step1'
import Step2 from './step2'
import Step3 from './step3'
import Step4 from './step4'
import Step5 from './step5'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const JobCreate = defineComponent({
  name: 'JobCreate',
  props,
  setup() {
    const { t } = useForm()
    const isEdit = ref(false)
    const currentRef = ref<number>(1)
    const next = () => {
      currentRef.value++
    }
    return {
      t,
      isEdit,
      currentRef,
      next
    }
  },
  render() {
    const { t, next } = this
    return (
      <div class={styles.container}>
        <div class={styles['table-container']}>
          <NSpace vertical>
            <NSpace>
              <NSteps current={this.currentRef} status='process'>
                <NStep title={t('synchronization.create_form.step1_title')}>
                  <div style='width: 25vh'></div>
                </NStep>
                <NStep title={t('synchronization.create_form.step2_title')}>
                  <div style='width: 25vh'></div>
                </NStep>
                <NStep title={t('synchronization.create_form.step3_title')}>
                  <div style='width: 25vh'></div>
                </NStep>
                <NStep title={t('synchronization.create_form.step4_title')}>
                  <div style='width: 25vh'></div>
                </NStep>
                <NStep title={t('synchronization.create_form.step5_title')}>
                  <div style='width: 25vh'></div>
                </NStep>
              </NSteps>
            </NSpace>
            <NSpace class={styles['create-form']}>
              {() => {
                switch (this.currentRef) {
                  case 1:
                    return <Step1></Step1>
                  case 2:
                    return <Step2></Step2>
                  case 3:
                    return <Step3></Step3>
                  case 4:
                    return <Step4></Step4>
                  case 5:
                    return <Step5></Step5>
                }
              }}
            </NSpace>
          </NSpace>
          <div></div>
        </div>
        <NSpace>
          <NButton>{t('common.negativeText')}</NButton>
          <NButton type='primary' onClick={next}>
            {t('synchronization.create_form.next_step')}
          </NButton>
        </NSpace>
      </div>
    )
  }
})

export default JobCreate
