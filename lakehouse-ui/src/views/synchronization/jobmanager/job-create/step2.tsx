/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import { NForm, NFormItem, NInput, NSpace } from 'naive-ui'
import { useForm } from './use-form'

const props = {
  value: {
    type: String as PropType<string>,
    default: ''
  },
  updateEdit: {
    type: Function as PropType<(v: any) => void>,
    default: () => {}
  }
}

const Step2 = defineComponent({
  name: 'Step2',
  props,
  setup() {
    const { t } = useForm()
    return {
      t
    }
  },
  render() {
    const { t } = this
    return (
      <NSpace>
        <NForm labelPlacement='left'>
          <NFormItem label={t('synchronization.job_name')}>
            <NInput></NInput>
          </NFormItem>
          <NFormItem label={t('synchronization.create_form.job_desc')}>
            <NInput type='textarea'></NInput>
          </NFormItem>
        </NForm>
      </NSpace>
    )
  }
})

export default Step2
