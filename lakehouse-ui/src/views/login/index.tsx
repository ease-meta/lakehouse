/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, toRefs, ref, onMounted } from 'vue'
import { NInput, NButton, NForm, NFormItem, NImage } from 'naive-ui'
import { useForm } from './use-form'
import { useLogin } from './use-login'
import styles from './index.module.scss'
import logo from '@/assets/images/logo.svg'
import cookies from 'js-cookie'
import { useLocalesStore } from '@/store/locales/locales'

const Login = defineComponent({
  name: 'Login',
  setup() {
    const { variables, t } = useForm()
    const { handleLogin } = useLogin(variables)
    const logoUrl = ref(logo)
    const localesStore = useLocalesStore()

    onMounted(() => {
      cookies.set('language', localesStore.getLocales, { path: '/' })
    })

    return { t, ...toRefs(variables), handleLogin, logoUrl }
  },
  render() {
    const { t, rules, loginForm, handleLogin, logoUrl } = this
    return (
      <div class={styles.container}>
        <div class={styles['login-model']}>
          <div class={styles.title}>
            <NImage
              class={styles['logo-img']}
              src={logoUrl}
              height='45'
              width='60'
              preview-disabled
            />
            <div class={styles['title-text']}>云原生大数据分析</div>
            <div class={styles['title-text']}>Lakehouse</div>
          </div>
          <div class={styles['form-model']}>
            <NForm
              class={styles['input-form']}
              rules={rules}
              ref='loginFormRef'
            >
              <NFormItem path='userName' style='height: 60px'>
                <NInput type='text' v-model={[loginForm.userName, 'value']} />
              </NFormItem>
              <NFormItem path='userPassword' style='height: 60px'>
                <NInput
                  type='password'
                  v-model={[loginForm.userPassword, 'value']}
                />
              </NFormItem>
            </NForm>
            <NButton
              class={styles['btn-login']}
              type='info'
              disabled={!loginForm.userName || !loginForm.userPassword}
              style={{ width: '100%' }}
              onClick={handleLogin}
            >
              {t('login.login')}
            </NButton>
          </div>
        </div>
      </div>
    )
  }
})

export default Login
