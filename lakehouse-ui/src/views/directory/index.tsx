/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import {
  NButton,
  NCard,
  NDataTable,
  NDatePicker,
  NForm,
  NFormItem,
  NGrid,
  NInput,
  NPagination,
  NSpace,
  NGridItem
} from 'naive-ui'
import { defineComponent, onMounted, toRefs, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import { useTable } from './use-table'
import CreateDatabaseModal from './components/create-modal'

const list = defineComponent({
  name: 'list',
  setup() {
    const { t } = useI18n()
    const { variables, createColumns, getList } = useTable()

    const clickCreate = () => {
      variables.showModalRef = true
    }

    const onCancelModal = () => {
      variables.showModalRef = false
    }

    onMounted(() => {
      createColumns(variables)
      getList()
    })

    watch(useI18n().locale, () => {
      createColumns(variables)
      getList()
    })

    return {
      t,
      clickCreate,
      onCancelModal,
      ...toRefs(variables)
    }
  },
  render() {
    const { t, onCancelModal, loadingRef } = this
    return (
      <div class={styles.container}>
        <div class={styles['table-container']}>
          <NCard>
            <NForm labelPlacement='left' label-width='auto'>
              <NGrid class={styles['search-header']} x-gap='20' cols='3'>
                <NGridItem>
                  <NFormItem
                    class={styles['search-input']}
                    label={t('directory.table_name')}
                  >
                    <NInput></NInput>
                  </NFormItem>
                </NGridItem>
                <NGridItem>
                  <NFormItem
                    class={styles['search-input']}
                    label={t('directory.database_name')}
                  >
                    <NInput></NInput>
                  </NFormItem>
                </NGridItem>
                <NGridItem>
                  <NFormItem
                    class={styles['search-input']}
                    label={t('directory.last_modifier_time')}
                  >
                    <NDatePicker type='datetimerange'></NDatePicker>
                  </NFormItem>
                </NGridItem>
                <NGridItem>
                  <NFormItem
                    class={styles['search-input']}
                    label={t('directory.ref_task_id')}
                  >
                    <NInput></NInput>
                  </NFormItem>
                </NGridItem>
                <NGridItem>
                  <NFormItem
                    class={styles['search-input']}
                    label={t('directory.ref_task_type')}
                  >
                    <NInput></NInput>
                  </NFormItem>
                </NGridItem>
                <NGridItem>
                  <NSpace class={styles['search-input']}>
                    <NButton type='info'>{t('common.search')}</NButton>
                    <NButton>{t('common.reset')}</NButton>
                  </NSpace>
                </NGridItem>
              </NGrid>
            </NForm>
          </NCard>
          <NSpace class={styles['table-header']} justify='space-between'>
            <div class={styles['header-buttons']}>
              <NButton style='margin-left: 10px'>{t('common.delete')}</NButton>
              <NButton style='margin-left: 10px'>{t('common.refresh')}</NButton>
            </div>
          </NSpace>
          <div class={styles['table-list']}>
            <NDataTable
              loading={loadingRef}
              columns={this.columns}
              data={this.tableData}
              scrollX={this.tableWidth}
            />
          </div>
          <div class={styles.pagination}>
            <NPagination
              v-model:page={this.page}
              v-model:page-size={this.pageSize}
              pageCount={this.totalPage}
              showSizePicker
              pageSizes={[10, 30, 50]}
              showQuickJumper
            />
          </div>
        </div>
        <CreateDatabaseModal
          showModalRef={this.showModalRef}
          onCancelModal={onCancelModal}
        ></CreateDatabaseModal>
      </div>
    )
  }
})

export default list
