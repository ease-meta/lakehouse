/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useI18n } from 'vue-i18n'
import { reactive, ref } from 'vue'
import { FormRules } from 'naive-ui'

export function useForm() {
  const { t } = useI18n()

  const getTableData = (variables: any) => {
    variables.tableData = []
  }

  const resetForm = () => {
    variables.model = {
      projectName: '',
      description: '',
      userName: ''
    }
  }

  const variables = reactive({
    createFormRef: ref(),
    columns: [],
    tableData: [],
    model: {
      projectName: '',
      description: '',
      userName: ''
    },
    saving: false,
    rules: {
      projectName: {
        required: true,
        trigger: ['input', 'blur'],
        validator() {
          if (variables.model.projectName === '') {
            return new Error(t('project.list.project_tips'))
          }
        }
      },
      userName: {
        required: true,
        trigger: ['input', 'blur'],
        validator() {
          if (variables.model.userName === '') {
            return new Error(t('project.list.username_tips'))
          }
        }
      }
    } as FormRules
  })

  const handleValidate = (statusRef: number) => {
    variables.createFormRef.validate((errors: any) => {
      if (!errors) {
        statusRef === 0 ? submitProjectModal() : updateProjectModal()
      } else {
        return
      }
    })
  }

  const submitProjectModal = async () => {
    if (variables.saving) return
    variables.saving = true
  }

  const updateProjectModal = async () => {
    if (variables.saving) return
    variables.saving = true
  }

  return {
    variables,
    t,
    handleValidate,
    getTableData,
    resetForm
  }
}
