/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType, toRefs, onMounted } from 'vue'
import {
  NModal,
  NForm,
  NFormItem,
  NInput,
  NCard,
  NButton,
  NAlert
} from 'naive-ui'
import { useForm } from './use-form'
import style from '../index.module.scss'

const props = {
  showModalRef: {
    type: Boolean as PropType<boolean>,
    default: false
  }
}

const CreateDatabaseModal = defineComponent({
  name: 'CreateDatabaseModal',
  props,
  emits: ['cancelModal', 'confirmModal'],
  setup(props, ctx) {
    const { variables, t, getTableData } = useForm()

    const cancelModal = () => {
      ctx.emit('cancelModal', props.showModalRef)
    }

    const confirmModal = () => {
      ctx.emit('confirmModal', props.showModalRef)
    }

    onMounted(() => {
      getTableData(variables)
    })

    return { ...toRefs(variables), t, cancelModal, confirmModal }
  },
  render() {
    const { t, cancelModal } = this
    return (
      <NModal show={this.showModalRef} onMaskClick={cancelModal}>
        <NCard
          title={t('database.create_database')}
          class={style['create-modal-card']}
          closable
          onClose={cancelModal}
        >
          {{
            default: () => (
              <div>
                <NAlert type='warning'>
                  <span>{t('database.create_database_tips')}</span>
                </NAlert>
                <NForm
                  class={style['create-modal-form']}
                  labelPlacement='left'
                  labelWidth='auto'
                  size='medium'
                >
                  <NFormItem label={t('database.name')}>
                    <NInput></NInput>
                  </NFormItem>
                  <NFormItem label={t('database.description')}>
                    <NInput></NInput>
                  </NFormItem>
                </NForm>
              </div>
            ),
            footer: () => (
              <div>
                <NButton>{t('instance.create_modal.cancel')}</NButton>
                <NButton>{t('instance.create_modal.submit')}</NButton>
              </div>
            )
          }}
        </NCard>
      </NModal>
    )
  }
})

export default CreateDatabaseModal
