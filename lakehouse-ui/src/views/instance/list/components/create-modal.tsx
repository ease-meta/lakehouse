/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType, toRefs, onMounted } from 'vue'
import {
  NModal,
  NForm,
  NFormItem,
  NInput,
  NCard,
  NRadioGroup,
  NRadioButton,
  NSpace,
  NDataTable,
  NButton
} from 'naive-ui'
import { useForm } from './use-form'
import style from '../index.module.scss'

const props = {
  showModalRef: {
    type: Boolean as PropType<boolean>,
    default: false
  }
}

const CreateModal = defineComponent({
  name: 'CreateModal',
  props,
  emits: ['cancelModal', 'confirmModal'],
  setup(props, ctx) {
    const {
      variables,
      t,
      createColumns,
      getTableData,
      handleValidate,
      resetForm
    } = useForm(props, ctx)

    const cancelModal = () => {
      ctx.emit('cancelModal', props.showModalRef)
      resetForm()
    }

    const confirmModal = () => {
      handleValidate()
    }

    onMounted(() => {
      createColumns(variables)
      getTableData(variables)
      resetForm()
    })

    return { ...toRefs(variables), t, cancelModal, confirmModal }
  },
  render() {
    const { t, columns, tableData, cancelModal, confirmModal, rules } = this
    return (
      <NModal show={this.showModalRef} onMaskClick={cancelModal}>
        <NCard
          class={style['create-modal-card']}
          closable
          onClose={cancelModal}
        >
          {{
            default: () => (
              <NForm
                labelPlacement='left'
                labelWidth='auto'
                size='medium'
                rules={rules}
                ref='createFormRef'
              >
                <NFormItem
                  path='instanceName'
                  label={t('instance.instance_name')}
                >
                  <NInput
                    v-model:value={this.createFormItem.instanceName}
                  ></NInput>
                </NFormItem>
                <NFormItem path='engineType' label={t('instance.engine_type')}>
                  <NRadioGroup v-model:value={this.createFormItem.engineType}>
                    <NSpace>
                      <NRadioButton
                        key={this.computeButtonData[0].value}
                        value={this.computeButtonData[0].value}
                        label={this.computeButtonData[0].value}
                      ></NRadioButton>
                    </NSpace>
                  </NRadioGroup>
                </NFormItem>
                <NFormItem
                  path='computeResourceSize'
                  label={t('instance.compute_resource_size')}
                  feedback={t('instance.create_modal.compute_resource_tips')}
                >
                  <NDataTable columns={columns} data={tableData}></NDataTable>
                </NFormItem>
              </NForm>
            ),
            footer: () => (
              <div class={style['modal-footer']}>
                <NSpace justify='end'>
                  <NButton onClick={cancelModal}>
                    {t('instance.create_modal.cancel')}
                  </NButton>
                  <NButton onClick={confirmModal} type='info'>
                    {t('instance.create_modal.submit')}
                  </NButton>
                </NSpace>
              </div>
            )
          }}
        </NCard>
      </NModal>
    )
  }
})

export default CreateModal
