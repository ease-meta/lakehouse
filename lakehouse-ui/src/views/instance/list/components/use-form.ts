/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useI18n } from 'vue-i18n'
import { h, reactive, ref, SetupContext } from 'vue'
import { FormRules, NRadio } from 'naive-ui'
import { createInstance } from '@/service/modules/instance'
import { useUserStore } from '@/store/user/user'
import { UserInfoDtos } from '@/service/modules/user/types'

export function useForm(
  props: any,
  ctx: SetupContext<('cancelModal' | 'confirmModal')[]>
) {
  const { t } = useI18n()
  const $userStore = useUserStore()

  const createColumns = (variables: any) => {
    variables.columns = [
      {
        title: '',
        key: 'select',
        render(row: any) {
          return h(NRadio, {
            onClick: () => handleRowSelect(row),
            checked: row.value === variables.selectSize
          })
        }
      },
      {
        title: t('instance.create_modal.size'),
        key: 'size'
      },
      {
        title: t('instance.create_modal.scene'),
        key: 'environment'
      }
    ]
  }

  const getTableData = (variables: any) => {
    variables.tableData = [
      {
        key: 1,
        size: t('instance.size_types.small'),
        value: 'SMALL',
        environment: t('instance.size_desc.small')
      },
      {
        key: 2,
        size: t('instance.size_types.medium'),
        value: 'MEDIUM',
        environment: t('instance.size_desc.medium')
      },
      {
        key: 3,
        size: t('instance.size_types.large'),
        value: 'LARGE',
        environment: t('instance.size_desc.large')
      },
      {
        key: 4,
        size: t('instance.size_types.xlarge'),
        value: 'XLARGE',
        environment: t('instance.size_desc.xlarge')
      }
    ]
  }

  const handleRowSelect = (row: any) => {
    variables.selectSize = row.value
    variables.createFormItem.computeResourceSize = row.value
  }

  const resetForm = () => {
    variables.createFormItem = {
      instanceName: '',
      engineType: 'Spark',
      computeResourceSize: 'SMALL'
    }
  }

  const variables = reactive({
    createFormRef: ref(),
    columns: [],
    tableData: [],
    selectSize: ref('SMALL'),
    computeButtonData: [
      {
        value: 'Spark',
        key: 'Spark'
      }
    ],
    createFormItem: {
      instanceName: '',
      engineType: '',
      computeResourceSize: ''
    },
    saving: false,
    rules: {
      instanceName: {
        required: true,
        trigger: ['input', 'blur'],
        validator() {
          if (variables.createFormItem.instanceName === '') {
            return new Error(t('instance.create_modal.instance_name_tips'))
          }
          if (
            variables.createFormItem.instanceName.search(
              new RegExp(/^[a-z][_0-9a-z]{3,31}$/)
            )
          ) {
            return new Error(
              t('instance.create_modal.instance_name_format_tips')
            )
          }
        }
      }
    } as FormRules
  })

  const handleValidate = () => {
    variables.createFormRef.validate((errors: any) => {
      if (!errors) {
        submitCreateModal()
      } else {
        return
      }
    })
  }

  const submitCreateModal = async () => {
    await createInstance({
      instance: variables.createFormItem.instanceName,
      clusterType: 'LAKEHOUSE',
      computeResourceSize: variables.createFormItem.computeResourceSize,
      engineType: variables.createFormItem.engineType,
      userName: ($userStore.getUserInfo as UserInfoDtos).userName
    })
    resetForm()
    ctx.emit('confirmModal', props.showModalRef)
  }

  return {
    variables,
    t,
    handleValidate,
    createColumns,
    getTableData,
    resetForm
  }
}
