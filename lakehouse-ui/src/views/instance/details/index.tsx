/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { NBreadcrumb, NBreadcrumbItem } from 'naive-ui'
import { defineComponent, onMounted, ref } from 'vue'
import { useI18n } from 'vue-i18n'
import styles from './index.module.scss'
import BaseInfo from './components/base-info'
import SystemConfig from './components/system-config'
import { useRoute } from 'vue-router'
import { getInstanceDetailsById } from '@/service/modules/instance'

const InstanceDetails = defineComponent({
  name: 'InstanceDetails',
  setup() {
    const route = useRoute()
    const { t } = useI18n()

    const instanceId = route.params.instanceId as string
    const instanceInfo = ref({})

    onMounted(async () => {
      const res = await getInstanceDetailsById(instanceId)
      instanceInfo.value = res
    })
    return {
      t,
      instanceInfo
    }
  },
  render() {
    const { t } = this
    return (
      <div class={styles.container}>
        <NBreadcrumb>
          <NBreadcrumbItem href='/instance'>
            {t('instance.instance_list')}
          </NBreadcrumbItem>
          <NBreadcrumbItem>{t('instance.instance_name')}</NBreadcrumbItem>
        </NBreadcrumb>
        <BaseInfo instanceInfo={this.instanceInfo}></BaseInfo>
        <SystemConfig instanceInfo={this.instanceInfo}></SystemConfig>
      </div>
    )
  }
})

export default InstanceDetails
