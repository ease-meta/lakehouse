/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent } from 'vue'
import { NCard, NSpace } from 'naive-ui'
import { useI18n } from 'vue-i18n'
import style from '../index.module.scss'
import { instanceStatusType, computeResourceSizeType } from '@/common/common'

const props = {
  instanceInfo: {
    type: Object,
    default: {},
    required: true
  }
}

const BaseInfo = defineComponent({
  name: 'BaseInfo',
  props,
  setup() {
    const { t } = useI18n()
    const instanceStatusTypeConfig = instanceStatusType(t)
    const computeResourceSizeTypeConfig = computeResourceSizeType(t)

    return {
      t,
      computeResourceSizeTypeConfig,
      instanceStatusTypeConfig
    }
  },
  render() {
    const { t, computeResourceSizeTypeConfig, instanceStatusTypeConfig } = this
    return (
      <NCard style='margin-top: 20px;'>
        <div class={style['base-info']}>
          <div class={style['baseinfo-title']}>
            {t('instance.base_info.base_info')}
          </div>
          <NSpace
            class={style['baseinfo-content']}
            itemStyle={{ width: '30%' }}
          >
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.instance_name')}
              </span>
              <span class={style.text}>
                {this.$props.instanceInfo.instance}
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.instance_id')}
              </span>
              <span class={style.text}>
                {this.$props.instanceInfo.instanceId}
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>{t('instance.base_info.status')}</span>
              <span class={style.text}>
                {
                  instanceStatusTypeConfig[
                    this.$props.instanceInfo
                      .status as keyof typeof instanceStatusTypeConfig
                  ]?.desc
                }
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.engine_type')}
              </span>
              <span class={style.text}>
                {this.$props.instanceInfo.engineType}
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.compute_resource_size')}
              </span>
              <span class={style.text}>
                {
                  computeResourceSizeTypeConfig[
                    this.$props.instanceInfo
                      .computeResourceSize as keyof typeof computeResourceSizeTypeConfig
                  ]?.desc
                }
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.create_user')}
              </span>
              <span class={style.text}>
                {this.$props.instanceInfo.createdBy}
              </span>
            </div>
            <div class={style['baseinfo-item']}>
              <span class={style.title}>
                {t('instance.base_info.create_time')}
              </span>
              <span class={style.text}>
                {this.$props.instanceInfo.createTime}
              </span>
            </div>
          </NSpace>
        </div>
      </NCard>
    )
  }
})

export default BaseInfo
