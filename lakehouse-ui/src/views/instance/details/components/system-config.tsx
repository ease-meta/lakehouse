/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent } from 'vue'
import { NCollapse, NCollapseItem, NCard, NSpace, NAlert } from 'naive-ui'
import style from '../index.module.scss'
import ShowEdit from '@/views/user/components/show-edit'
import { useI18n } from 'vue-i18n'

const props = {
  instanceInfo: {
    type: Object,
    default: {},
    required: true
  }
}

const SystemConfig = defineComponent({
  name: 'SystemConfig',
  props,
  setup() {
    const { t } = useI18n()
    return {
      t
    }
  },
  render() {
    const { t } = this
    return (
      <NCard style='margin-top: 20px;'>
        <div class={style['system-config']}>
          <NCollapse arrowPlacement='right'>
            <NCollapseItem title={t('instance.system_config.system_config')}>
              <NAlert type='warning'>
                <span>{t('instance.system_config.system_config_tips1')}</span>
                <br />
                <span>{t('instance.system_config.system_config_tips2')}</span>
              </NAlert>
              <NSpace class={style['parallelize-set']}>
                <div>{t('instance.system_config.max_parallelize_num')}</div>
                <ShowEdit
                  value={this.$props.instanceInfo.taskParallelize}
                ></ShowEdit>
              </NSpace>
            </NCollapseItem>
          </NCollapse>
        </div>
      </NCard>
    )
  }
})

export default SystemConfig
