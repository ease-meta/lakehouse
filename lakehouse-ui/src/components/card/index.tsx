/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import { NCard } from 'naive-ui'

const headerStyle = {
  borderBottom: '1px solid var(--n-border-color)'
}

const contentStyle = {
  padding: '8px 10px'
}

const props = {
  title: {
    type: String as PropType<string>
  }
}

const Card = defineComponent({
  name: 'Card',
  props,
  render() {
    const { title, $slots } = this
    return (
      <NCard
        title={title}
        size='small'
        headerStyle={headerStyle}
        contentStyle={contentStyle}
      >
        {$slots}
      </NCard>
    )
  }
})

export default Card
