/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { reactive, h } from 'vue'
import { NIcon } from 'naive-ui'
import { useI18n } from 'vue-i18n'
import {
  HomeOutlined,
  UserOutlined,
  KeyOutlined,
  LogoutOutlined,
  DatabaseOutlined,
  RightSquareOutlined
} from '@vicons/antd'
import {
  AlignSpaceEvenlyVertical20Regular,
  WindowConsole20Filled
} from '@vicons/fluent'

export function useDataList() {
  const { t } = useI18n()

  const renderIcon = (icon: any) => {
    return () => h(NIcon, null, { default: () => h(icon) })
  }

  const localesOptions = [
    {
      label: 'English',
      key: 'en_US'
    },
    {
      label: '中文',
      key: 'zh_CN'
    }
  ]

  const state = reactive({
    localesOptions,
    menuOptions: [],
    userDropdownOptions: []
  })

  const changeMenuOption = (state: any) => {
    state.menuOptions = [
      {
        label: t('menu.home'),
        key: 'home',
        icon: renderIcon(HomeOutlined)
      },
      {
        label: t('menu.instance'),
        key: 'instance',
        icon: renderIcon(AlignSpaceEvenlyVertical20Regular)
      },
      {
        label: t('menu.synchronization'),
        key: 'synchronization',
        icon: renderIcon(RightSquareOutlined)
      },
      {
        label: t('menu.data_manage'),
        key: 'manage',
        icon: renderIcon(DatabaseOutlined),
        children: [
          {
            label: t('menu.database'),
            key: 'database'
          },
          {
            label: t('menu.directory'),
            key: 'directory'
          },
          {
            label: t('menu.discovery'),
            key: 'discovery'
          }
        ]
      },
      {
        label: t('menu.explore'),
        key: 'explore',
        icon: renderIcon(WindowConsole20Filled),
        children: [
          {
            label: t('menu.sqlconsole'),
            key: 'sqlconsole'
          }
        ]
      },
      {
        label: t('menu.user'),
        key: 'user',
        icon: renderIcon(UserOutlined)
      }
    ]
  }

  const changeUserDropdown = (state: any) => {
    state.userDropdownOptions = [
      {
        label: t('userDropdown.profile'),
        key: 'profile',
        icon: renderIcon(UserOutlined)
      },
      {
        label: t('userDropdown.password'),
        key: 'password',
        icon: renderIcon(KeyOutlined)
      },
      {
        label: t('userDropdown.logout'),
        key: 'logout',
        icon: renderIcon(LogoutOutlined)
      }
    ]
  }

  return {
    state,
    changeMenuOption,
    changeUserDropdown
  }
}
