/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { DropdownOption } from 'naive-ui'
import { useI18n } from 'vue-i18n'
import cookies from 'js-cookie'
import { useLocalesStore } from '@/store/locales/locales'
import type { Locales } from '@/store/locales/types'

export function useDropDown(chooseVal: any) {
  const { locale } = useI18n()
  const localesStore = useLocalesStore()

  const handleSelect = (key: string | number, option: DropdownOption) => {
    // console.log(key, option)
    chooseVal.value = option.label
    locale.value = key as Locales
    localesStore.setLocales(locale.value as Locales)
    cookies.set('language', locale.value, { path: '/' })
  }
  return {
    handleSelect
  }
}
