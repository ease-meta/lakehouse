/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useRouter } from 'vue-router'
import type { Router } from 'vue-router'
import { DropdownOption } from 'naive-ui'
import { logout } from '@/service/modules/user'
import { useUserStore } from '@/store/user/user'

export function useDropDown() {
  const router: Router = useRouter()
  const $userStore = useUserStore()

  const handleSelect = (key: string | number, unused: DropdownOption) => {
    if (key === 'logout') {
      useLogout()
    } else if (key === 'password') {
      router.push({ path: '/password' })
    } else if (key === 'profile') {
      router.push({ path: '/profile' })
    }
  }

  const useLogout = () => {
    logout().then(() => {
      $userStore.setSessionId('')
      $userStore.setUserInfo({})

      router.push({ path: '/login' })
    })
  }

  return {
    handleSelect
  }
}
