/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent, PropType } from 'vue'
import styles from './index.module.scss'
import Logo from '../logo'

const SideBar = defineComponent({
  name: 'SideBar',
  props: {
    localesOptions: {
      type: Array as PropType<any>,
      default: []
    }
  },
  emits: ['handleMenuClick'],
  setup() {},
  render() {
    return (
      <div class={styles.container}>
        <Logo />
      </div>
    )
  }
})

export default SideBar
