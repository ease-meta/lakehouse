/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { useRouter } from 'vue-router'
import type { Router } from 'vue-router'
import { MenuOption } from 'naive-ui'
import { SetupContext } from 'vue'

export function useMenuClick(ctx: SetupContext<'handleMenuClick'[]>) {
  const router: Router = useRouter()

  const handleMenuClick = (key: string, item: MenuOption) => {
    ctx.emit('handleMenuClick', item)
    // explore sqlconsole need open new window
    if (key.endsWith('sqlconsole')) {
      // TODO: need fix window open can not debug correctly
      router.push({ path: '/explore/sqlconsole' })
    } else {
      router.push({ path: `/${key}` })
    }
  }

  return {
    handleMenuClick
  }
}
