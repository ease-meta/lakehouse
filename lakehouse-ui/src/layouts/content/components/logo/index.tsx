/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { defineComponent } from 'vue'
import styles from './index.module.scss'

const Logo = defineComponent({
  name: 'Logo',
  setup() {},
  render() {
    return (
      <div class={styles.logo}>
        <div class={styles['logo-img']} />
        <div class={styles.title}>
          <div>云原生大数据分析</div>
          <div>LakeHouse</div>
        </div>
      </div>
    )
  }
})

export default Logo
