/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios } from '@/service/service'
import { METADATA_API } from '@/service/service'

/**
 * deleteDirectorySingleReq
 * @param databaseName
 * @param tableName
 * @returns
 */
export function deleteDirectorySingleReq(
  databaseName: string,
  tableName: string
): Promise<boolean> {
  const url = `/databases/${databaseName}/tables/${tableName}`
  return axios.delete(url)
}

export function getDirectoryList(body: any): Promise<any> {
  const url = METADATA_API + 'user/tables/search'
  return axios.post(url, body)
}
