/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios, LOGIN_API, USER_API } from '@/service/service'
import { UserInfoDtos } from './types'

/**
 * login
 * @param userName
 * @param userPassword
 * @returns
 */
export function login(userName: string, userPassword: string): Promise<string> {
  const url = LOGIN_API + 'login'
  const params = {
    userName,
    userPassword
  }
  return axios.post<never, string>(url, null, { params: params })
}

/**
 * logout
 * @returns
 */
export function logout(): Promise<null> {
  const url = LOGIN_API + 'signOut'
  return axios.post<never, null>(url, null)
}

/**
 * get current user info
 * @returns
 */
export function getCurrentUserInfo(): Promise<UserInfoDtos> {
  const url = LOGIN_API + 'get-current-user'
  return axios.get(url)
}

/**
 * create user
 * @param userName
 * @param userPassword
 * @param description
 * @returns
 */
export function createUser(
  userName: string,
  userPassword: string,
  description: string
): Promise<UserInfoDtos> {
  const url = USER_API + 'create'
  const params = {
    userName,
    userPassword,
    description
  }
  return axios.post<never, UserInfoDtos>(url, null, { params: params })
}

/**
 * update user description
 * @param id
 * @param description
 * @returns
 */
export function updateUser(id: string, description: string): Promise<null> {
  const url = USER_API + 'update'
  const params = {
    id,
    description
  }
  return axios.post(url, null, { params: params })
}

/**
 * delete user
 * @param id
 * @returns
 */
export function deleteUser(id: string): Promise<null> {
  const url = USER_API + 'delete'
  const params = {
    id
  }
  return axios.post(url, null, { params: params })
}

/**
 * batch delete user
 * @param ids
 * @returns
 */
export function batchDeleteUser(ids: string) {
  const url = USER_API + 'batch-delete'
  const params = {
    ids
  }
  return axios.post(url, null, { params: params })
}

/**
 * get user info
 * @param id
 * @returns
 */
export function getUserInfo(id: string): Promise<UserInfoDtos> {
  const url = USER_API + 'get-user-info'
  const params = {
    id
  }
  return axios.post<never, UserInfoDtos>(url, null, { params: params })
}

/**
 * query user list paging
 * @param pageNo
 * @param pageSize
 * @param searchVal
 * @returns
 */
export function queryUserListPaging(
  pageNo: number,
  pageSize: number,
  searchVal: string | null
): Promise<UserInfoDtos[]> {
  const url = USER_API + 'list-paging'
  const params = {
    pageNo,
    pageSize,
    searchVal
  }
  return axios.get(url, { params: params })
}

/**
 * reset password
 * @param id
 * @param userPassword
 * @param resetPassword
 * @returns
 */
export function resetPassword(id: string, userPassword: string): Promise<null> {
  const url = USER_API + 'reset-password'
  const params = {
    id,
    userPassword
  }
  return axios.post(url, null, { params: params })
}
