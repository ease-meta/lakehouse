/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

enum USER_TYPE {
  GENERAL_USER = 'GENERAL_USER',
  ADMIN_USER = 'ADMIN_USER'
}

export interface UserInfoDtos {
  userId: string
  userName: string
  userPassword: string
  userType: USER_TYPE // GENERAL_USER || ADMIN_USER
  createTime: Date
  updateTime: Date
  description: string
}
