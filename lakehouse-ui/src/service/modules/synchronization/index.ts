/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { axios } from '@/service/service'

/**
 * run job
 * @param id
 * @returns
 */
export function runJob(id: string): Promise<boolean> {
  const url = `jobs/run/${id}`
  return axios.get(url)
}

/**
 * delete job
 * @param ids
 * @returns
 */
export function deleteJob(ids: string): Promise<boolean> {
  const url = `jobs/delbatch?ids=${ids}`
  return axios.delete(url)
}
