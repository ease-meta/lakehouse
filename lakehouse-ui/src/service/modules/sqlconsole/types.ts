/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import { TreeOption } from 'naive-ui'

export interface ScopedSlots {
  title: string
  parentLabel: string
}

// db/table/column tree
export interface MetadataTree extends TreeOption {
  key: number
  label: string
  scopedSlots: ScopedSlots /** tree extend message */
  children: MetadataTree[]
}

export interface SqlQueryJobItemDtos {
  jobId: string
  sql: string
  message: string | null
  result: string | null
}

// sql exec result
export interface SqlExecResult {
  sqlQueryJobItemDtos: SqlQueryJobItemDtos[]
}

// abort status
export interface SqlAbortResultItemDtos {
  commitSuccessed: boolean
  jobId: string
  message: string
}

// abort exec result
export interface SqlAbortResult {
  sqlAbortResultItemDtos: SqlAbortResultItemDtos[]
}

// result from job query
export interface JobExecResult {
  affectedCount: string
  affectedRows: string
  columnList: string[]
  errorMsg: string
  executeTime: string
  originColumnList?: any
  querySql: string
  resultSet: string[][]
  status: string
  syncErrorMsg: string
  totalColumnCount: string
  totalDataCount: string
  userId: string
}

// sql query module

// SQL Console Query Tab
export interface SqlQueryTab {
  name: string
  instance: string // dbname
  label: string
  closable: boolean
  bottomDivHeight: number
}

export type SqlTabs = SqlQueryTab[]

// exec job history
export interface HistoryExecJob {
  engineType: string
  finishTime: string
  instance: string
  runTime: number
  sqlContent: string
  status: string
  submitTime: string
  submitUser: string
  taskId: string
  taskType: string
  waitTime: number
}

export type HistoryExecResult = {
  totalList: HistoryExecJob[]
  total: number
}
