/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.hive;

import com.chinamobile.cmss.lakehouse.common.dto.TableNameDto;
import com.chinamobile.cmss.lakehouse.dao.entity.MetadataDatabaseEntity;

import java.util.List;
import java.util.Set;

import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.Table;

public interface HiveAccessService {

    public interface FunctionWithException<T, R> {
        @SuppressWarnings("PMD.AbstractMethodOrInterfaceMethodMustUseJavadocRule")
        R apply(T t) throws Exception;
    }

    public boolean isDatabaseExist(String databaseName);

    boolean isTableExist(String databaseName, String table, String userName);

    public void accessPermissionCheck(String userName, TableNameDto tableNameDto);

    List<Table> getTables(String dataBaseName, String userName);

    Table getTable(String databaseName, String tableName, String userName);

    void deleteTable(String userName, TableNameDto tableNameDto);

    Set<String> getDatabasesByUser(String userName);

    HiveSqlExecutor getHiveSqlExecutor(String userName);

    <T> T loanHiveMetaStoreClient(String userName, FunctionWithException<HiveMetaStoreClient, T> loan);

    void createDb(MetadataDatabaseEntity database, String userName);

    void deleteDb(MetadataDatabaseEntity database, String userName);

    boolean executeSql(String userName, String sql);
}
