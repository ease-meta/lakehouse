/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.quartz;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.Trigger;

public interface QuartzExecutor {

    /**
     * Add job to quartz
     * @param clazz job class name
     * @param jobName job name
     * @param jobGroupName job group name
     * @param cronExpression cron expression
     */
    void addJob(Class<? extends Job> clazz, String jobName, String jobGroupName, String cronExpression);

    /**
     * Add job with trigger and dataMap to quartz
     * @param clazz job class name
     * @param jobName job name
     * @param jobGroupName job group name
     * @param cronExpression cron expression
     * @param jobDataMap job config data map
     */
    void addJob(Class<? extends Job> clazz, String jobName, String jobGroupName, Trigger cronExpression, JobDataMap jobDataMap);

    /**
     * Delete job
     * @param jobName job name
     * @param jobGroupName job group name
     * @return
     */
    boolean deleteJob(String jobName, String jobGroupName);
}
