/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.sqlconsole.impl;

import com.chinamobile.cmss.lakehouse.common.dto.engine.LakehouseResponse;
import com.chinamobile.cmss.lakehouse.service.sqlconsole.JdbcKillTaskService;
import com.chinamobile.cmss.lakehouse.service.sqlconsole.config.JDBCUserConfigurations;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class JdbcKillTaskServiceImpl implements JdbcKillTaskService {

    private final HashMap<String, JDBCUserConfigurations> jdbcUserConfigurationsMap = new HashMap<>();

    @Override
    public LakehouseResponse cancelJdbcSQLTask(String taskId) {
        return null;
    }

    @Override
    public JDBCUserConfigurations getJDBCConfiguration(String user) {
        JDBCUserConfigurations jdbcUserConfigurations = jdbcUserConfigurationsMap.get(user);

        if (jdbcUserConfigurations == null) {
            jdbcUserConfigurations = new JDBCUserConfigurations();
            jdbcUserConfigurationsMap.put(user, jdbcUserConfigurations);
        }

        return jdbcUserConfigurations;
    }
}
