/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.service.hive;

import java.sql.Connection;
import java.util.Collection;
import java.util.NoSuchElementException;

import org.apache.commons.pool2.DestroyMode;
import org.apache.commons.pool2.KeyedObjectPool;

public class HiveConnectionPool implements KeyedObjectPool<String, Connection> {

    private KeyedObjectPool<String, Connection> hivePool;

    public HiveConnectionPool(KeyedObjectPool<String, Connection> hivePool) {
        this.hivePool = hivePool;
    }

    @Override
    public void addObject(String s) throws Exception, IllegalStateException, UnsupportedOperationException {
        hivePool.addObject(s);
    }

    @Override
    public void addObjects(Collection<String> keys, int count) throws Exception, IllegalArgumentException {
        hivePool.addObjects(keys, count);
    }

    @Override
    public void addObjects(String key, int count) throws Exception, IllegalArgumentException {
        hivePool.addObjects(key, count);
    }

    @Override
    public Connection borrowObject(String s) throws Exception, NoSuchElementException, IllegalStateException {
        return hivePool.borrowObject(s);
    }

    @Override
    public void clear() throws Exception, UnsupportedOperationException {
        hivePool.clear();
    }

    @Override
    public void clear(String s) throws Exception, UnsupportedOperationException {
        hivePool.clear(s);
    }

    @Override
    public void close() {
        hivePool.close();
    }

    @Override
    public int getNumActive() {
        return hivePool.getNumActive();
    }

    @Override
    public int getNumActive(String s) {
        return hivePool.getNumActive(s);
    }

    @Override
    public int getNumIdle() {
        return hivePool.getNumIdle();
    }

    @Override
    public int getNumIdle(String s) {
        return hivePool.getNumIdle(s);
    }

    @Override
    public void invalidateObject(String s, Connection connection) throws Exception {
        hivePool.invalidateObject(s, connection);
    }

    @Override
    public void invalidateObject(String key, Connection obj, DestroyMode mode) throws Exception {
        hivePool.invalidateObject(key, obj, mode);
    }

    @Override
    public void returnObject(String s, Connection connection) throws Exception {
        hivePool.returnObject(s, connection);
    }
}
