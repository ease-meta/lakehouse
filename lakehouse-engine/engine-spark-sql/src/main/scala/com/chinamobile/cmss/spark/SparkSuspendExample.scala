/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.spark

import java.util.UUID

import org.apache.spark.sql.SparkSession

object SparkSuspendExample {
  def main(args: Array[String]): Unit = {
    val name = s"spark-suspend-${UUID.randomUUID().toString}"
    val spark = SparkSession.builder().appName(name).getOrCreate()
    val suspendTime = if (args.size > 0) args(0).toInt else 60
    Thread.sleep(suspendTime * 1000)

    val df = spark.createDataFrame(Array((1, 2, 3, 4), (2, 4, 6, 8)))
    df.show()

    spark.stop()
  }
}