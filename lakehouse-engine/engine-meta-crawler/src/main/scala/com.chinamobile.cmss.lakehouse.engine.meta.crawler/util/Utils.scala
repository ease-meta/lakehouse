/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.util

import java.util.UUID

object Utils {
  private val chars = ((for (i <- 0 until 26)
    yield ('a' + i).toChar) union (for (i <- 0 until 26)
    yield ('A' + i).toChar) union (for (i <- 0 to 9) yield i)).toArray

  def generateShortUuid(): String = {
    val uuid = UUID.randomUUID().toString().replace("-", "");
    (for (i <- 0 until 8) yield i)
      .map(i => Integer.parseInt(uuid.substring(i * 4, i * 4 + 4), 16))
      .map(i => chars(i % 0x3e))
      .mkString
  }

  def surround(surroundStr: String, str: String): String =
    surround(surroundStr, str, surroundStr)

  def surround(left: String, str: String, right: String): String =
    left + str + right
}
