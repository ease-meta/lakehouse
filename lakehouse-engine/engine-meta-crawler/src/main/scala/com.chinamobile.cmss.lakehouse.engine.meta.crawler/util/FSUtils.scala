/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.util

import com.chinamobile.cmss.lakehouse.engine.meta.crawler.OssSource
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

object FSUtils {
  def getFileSystem(ossSource: OssSource, path: Path): FileSystem = {
    val conf = getConfigurationFromOssSource(ossSource)
    path.getFileSystem(conf)
  }

  def getConfigurationFromOssSource(ossSource: OssSource): Configuration = {
    val conf = new Configuration()
    conf.set("fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
    conf.set("fs.file.impl", "org.apache.hadoop.fs.LocalFileSystem")
    conf.set("fs.s3a.connection.ssl.enabled", "false")
    conf.set("fs.s3a.path.style.access", "true")
    conf.set("fs.s3a.endpoint", ossSource.endpoint)
    conf.set("fs.s3a.access.key", ossSource.ak)
    conf.set("fs.s3a.secret.key", ossSource.sk)
    conf
  }

  def wrapS3Path(path: String): Path = {
    wrapPath("s3a", path)
  }

  def wrapPath(prefix: String, path: String): Path = {
    var p =
      if (path.startsWith("/")) {
        s"$prefix:/" + path
      } else if (!path.startsWith(prefix)) {
        s"$prefix://" + path
      } else {
        path
      }
    if (!p.endsWith("/")) {
      p = p + "/"
    }
    new Path(p)
  }
}
