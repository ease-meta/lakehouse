/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.model

case class MayBeTable(
                       tableName: String,
                       tablePath: String,
                       partitionPaths: List[String]
                     )

case class Table(
                  tableName: String,
                  tablePath: String,
                  columns: List[ColumnInfo],
                  partitions: List[ColumnInfo],
                  partitionValues: List[PartitionValue]
                )

case class PartitionValue(path: String, values: List[String])

case class ColumnInfo(name: String, columnType: ColumnType)

trait ColumnType extends Serializable {
  def hiveType: String
}

object IntType extends ColumnType {
  override def hiveType: String = "INT"
}

object LongType extends ColumnType {
  override def hiveType: String = "INT"
}

object FloatType extends ColumnType {
  override def hiveType: String = "FLOAT"
}

object DoubleType extends ColumnType {
  override def hiveType: String = "DOUBLE"
}

object BigIntType extends ColumnType {
  override def hiveType: String = "BIGINT"
}

object BinaryType extends ColumnType {
  override def hiveType: String = "BINARY"
}

object BooleanType extends ColumnType {
  override def hiveType: String = "BOOLEAN"
}

object StringType extends ColumnType {
  override def hiveType: String = "STRING"
}

object DateType extends ColumnType {
  override def hiveType: String = "DATE"
}

object TimeStampType extends ColumnType {
  override def hiveType: String = "TIMESTAMP"
}

case class DecimalType(precision: Int, scala: Int) extends ColumnType {
  override def hiveType: String = s"DECIMAL($precision,$scala)"
}
