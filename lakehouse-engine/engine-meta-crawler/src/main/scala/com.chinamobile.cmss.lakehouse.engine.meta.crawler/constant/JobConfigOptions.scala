/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.engine.meta.crawler.constant

import org.apache.flink.configuration.ConfigOptions


object JobConfigOptions {
  val JDBC_DRIVER_NAME = ConfigOptions.key("jdbc.driver.name").stringType().noDefaultValue()
  val JDBC_CONNECTION_URL = ConfigOptions.key("jdbc.connection.url").stringType().noDefaultValue()
  val JDBC_CONNECTION_USERNAME = ConfigOptions.key("jdbc.connection.username").stringType().noDefaultValue()
  val JDBC_CONNECTION_PASSWORD = ConfigOptions.key("jdbc.connection.password").stringType().noDefaultValue()
  val JOB_INFO_TABLE_NAME = ConfigOptions.key("job.info.table.name").stringType().noDefaultValue()
  val JOB_CONFIG_COLUMN_NAME = ConfigOptions.key("job.config.column.name").stringType().noDefaultValue()
  val JOB_MONITOR_URL = ConfigOptions.key("job.monitor.url").stringType().noDefaultValue()
  val JOB_MONITOR_URL_COLUMN_NAME = ConfigOptions.key("job.monitor.column.name").stringType().noDefaultValue()
  val KUBERNETES_CLUSTER_ID = ConfigOptions.key("kubernetes.cluster-id").stringType().noDefaultValue()
  val SAVE_POINT_PATH = ConfigOptions.key("save.point.path").stringType().noDefaultValue()
  val PLUGIN_ROOT_DIR = ConfigOptions.key("plugin.root.dir").stringType().defaultValue("/opt/flink/syncplugins")
}
