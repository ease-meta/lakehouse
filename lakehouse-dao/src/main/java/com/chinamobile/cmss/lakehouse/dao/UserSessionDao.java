/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.dao.entity.UserSessionEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserSessionDao extends JpaRepository<UserSessionEntity, Integer> {

    /**
     * delete by user id and ip
     *
     * @param userId user id
     * @param ip     ip
     * @return
     */
    List<UserSessionEntity> findByUserIdAndIp(String userId, String ip);

    /**
     * delete by user id and ip
     *
     * @param userId user id
     * @param ip     ip
     */
    @Modifying
    @Transactional
    void deleteByUserIdAndIp(String userId, String ip);

    /**
     * find by uuid
     *
     * @param uuid uuid
     * @return user session
     */
    UserSessionEntity findByUuid(String uuid);
}
