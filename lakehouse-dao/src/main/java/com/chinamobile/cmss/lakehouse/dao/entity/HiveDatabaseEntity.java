/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "hive_dbs")
@NoArgsConstructor
@AllArgsConstructor
public class HiveDatabaseEntity {

    @Id
    @Column(name = "DB_ID")
    private long dbId;

    @Column(name = "CTLG_NAME")
    private String ctlgName;

    @Column(name = "CREATE_TIME")
    private Long createTime;

    @Column(name = "DESC")
    private String desc;

    @Column(name = "DB_LOCATION_URI")
    private String dbLocationUrl;

    @Column(name = "DB_MANAGED_LOCATION_URI")
    private String dbManagedLocationUrI;

    @Column(name = "NAME")
    private String name;

    @Column(name = "OWNER_NAME")
    private String ownerName;

    @Column(name = "OWNER_TYPE")
    private String ownerType;
}
