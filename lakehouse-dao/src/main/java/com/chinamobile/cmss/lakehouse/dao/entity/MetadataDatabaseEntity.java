/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "metadata_dbs")
public class MetadataDatabaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "bigint(20) COMMENT 'Id'")
    private Long id;

    @Column(name = "name", columnDefinition = "varchar(255) COMMENT 'name'")
    private String name;

    @Column(name = "description", columnDefinition = "varchar(255) COMMENT 'description'")
    private String description;

    @Column(name = "creator", columnDefinition = "varchar(255) COMMENT 'creator'")
    private String creator;

    @Column(name = "create_time", columnDefinition = "int(11) COMMENT 'create_time'")
    private Integer createTime;

    @Column(name = "updator", columnDefinition = "varchar(255) COMMENT 'updator'")
    private String updator;

    @Column(name = "update_time", columnDefinition = "int(11) COMMENT 'update_time'")
    private Integer updateTime;

    @Column(name = "deleted", columnDefinition = "bit(1) COMMENT 'deleted or not'")
    private Boolean deleted;

    @Transient
    private Long tableCount;
}
