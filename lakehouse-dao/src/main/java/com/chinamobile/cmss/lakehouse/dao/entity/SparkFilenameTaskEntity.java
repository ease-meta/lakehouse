/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Entity
@Data
@Table(name = "spark_filename_task")
public class SparkFilenameTaskEntity {
    /**
     * primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * file id
     */
    @Column(name = "file_id")
    private String fileId;

    /**
     * file name
     */
    @Column(name = "filename")
    private String filename;

    /**
     * task id
     */
    @Column(name = "task_id")
    private String taskId;

    /**
     * config
     */
    @Column(name = "config_content")
    private String configContent;

    @ApiModelProperty("creator")
    @Column(name = "create_user")
    private String createUser;

    @ApiModelProperty("creator user")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty("scheduleModel")
    @Column(name = "schedule_model")
    private String scheduleModel;

    @ApiModelProperty("relatedTaskId")
    @Column(name = "related_task_id")
    private String relatedTaskId;

    @ApiModelProperty("relatedTaskName")
    @Column(name = "related_task_name")
    private String relatedTaskName;


}
