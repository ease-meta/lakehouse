/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.dao.entity.HiveTableEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HiveTableDao extends JpaRepository<HiveTableEntity, Long>, HiveTableCustomDao {

    /**
     * getTablesByDbName
     *
     * @param dbName
     * @return
     */
    @Query(value = "select * from hive_table a left join dbs b on a.DB_ID = b.DB_ID where b.NAME = ?1", nativeQuery = true)
    List<HiveTableEntity> getTablesByDbName(String dbName);

    /**
     * getTableCountByDbName
     *
     * @param dbName
     * @return
     */
    @Query(value = "select count(*) from hive_table a left join dbs b on a.DB_ID = b.DB_ID where b.NAME = ?1", nativeQuery = true)
    long getTableCountByDbName(String dbName);

}
