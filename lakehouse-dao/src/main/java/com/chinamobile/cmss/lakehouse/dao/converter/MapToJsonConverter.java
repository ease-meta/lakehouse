/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.converter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeConverter;

import com.google.gson.Gson;

public class MapToJsonConverter implements AttributeConverter<Map<String, String>, String>, Serializable {
    private Gson gson = new Gson();

    @Override
    public String convertToDatabaseColumn(Map<String, String> m) {
        return m == null || m.size() == 0 ? null : gson.toJson(m);
    }

    @Override
    public Map<String, String> convertToEntityAttribute(String s) {
        return s == null ? new HashMap<>() : gson.fromJson(s, Map.class);
    }
}
