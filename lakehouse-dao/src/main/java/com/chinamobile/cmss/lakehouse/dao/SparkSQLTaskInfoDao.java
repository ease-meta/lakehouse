/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao;

import com.chinamobile.cmss.lakehouse.dao.entity.EngineTaskInfoEntity;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SparkSQLTaskInfoDao extends JpaRepository<EngineTaskInfoEntity, Integer>, JpaSpecificationExecutor<EngineTaskInfoEntity> {

    @Query(
        value = "select * from engine_task_info where engine_type='spark' and status in ('SUBMITTED', 'RUNNING')",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findUnFinishedTasks();

    @Query(
        value = "select * from engine_task_info where engine_type='spark' and status ='ACCEPTED' order by CREATE_TIME limit 10",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findAcceptedTasks();

    @Query(
        value = "select * from engine_task_info where engine_type='spark' and status ='SUBMIT_FAILED'",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findSubmitFailedTasks();

    List<EngineTaskInfoEntity> findByTaskIdIn(List<String> taskIds);

    EngineTaskInfoEntity findByTaskId(String taskId);

    List<EngineTaskInfoEntity> findAllByTaskId(String taskId);

    @Query(
        value = "select count(task_id) from engine_task_info where (TO_DAYS(now())=TO_DAYS(submit_time)) and instance in ?1",
        nativeQuery = true)
    int countAllTasks(List<String> instanceList);

    @Query(
        value = "select * from engine_task_info where engine_type='spark' and status in ('CANCELLED', 'FINISHED') and instance = ?1",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findFinishedTasks(String instance);

    @Query(
        value = "select * from engine_task_info where engine_type='spark' and status in ('CANCELLED', 'FINISHED') and instance = ?1 and finish_time < ?2",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findFinishedTasksBeforeTime(String instance, Date date);

    @Query(value = "select * from engine_task_info where engine_type='spark' and status = 'FAILED'",
        nativeQuery = true)
    List<EngineTaskInfoEntity> findAllFailedTasks();

    @Query(
        value = "select count(task_id) from engine_task_info where engine_type='spark' and status = 'RUNNING' and instance = ?1",
        nativeQuery = true)
    int countRunningTask(String instance);

    @Query(
        value = "select count(task_id) from engine_task_info where status <> 'SUBMIT_FAILED' and instance = ?1 and create_time > ?2",
        nativeQuery = true)
    int countSubmittedTaskAfterDate(String instance, Date date);

    @Query(
        value = "select count(task_id) from engine_task_info where status = 'FINISHED' and instance = ?1 and create_time > ?2",
        nativeQuery = true)
    int countFinishedTaskAfterDate(String instance, Date date);

    @Transactional
    void deleteAllByInstance(String instance);
}