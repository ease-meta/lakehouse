/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "hive_table")
public class HiveTableEntity {
    @Column(name = "TBL_ID")
    @Id
    private long tblId;

    @Column(name = "CREATE_TIME")
    private Integer createTime;

    @ManyToOne
    @JoinColumn(name = "DB_ID", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    private HiveDatabaseEntity hiveDatabaseEntity;

    @ManyToOne
    @JoinColumn(name = "SD_ID", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    private SdsEntity sdsEntity;

    @Column(name = "LAST_ACCESS_TIME")
    private Integer lastAccessTIme;

    @Column(name = "OWNER")
    private String owner;

    @Column(name = "OWNER_TYPE")
    private String ownerType;

    @Column(name = "RETENTION")
    private String retention;

    @Column(name = "IS_REWRITE_ENABLED")
    private boolean isRewriteEnabled;

    @Column(name = "TBL_NAME")
    private String tableName;

    @Column(name = "TBL_TYPE")
    private String tableType;
}
