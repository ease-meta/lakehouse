/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.dao.entity;

import com.chinamobile.cmss.lakehouse.common.enums.ClusterTypeEnum;
import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

@Data
@ToString
@Entity
@Table(name = "cluster_info")
public class LakehouseClusterInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @Column(name = "instance_id")
    private String instanceId;

    @Column(name = "instance", unique = true)
    private String instance;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "cluster_type")
    private String clusterType = ClusterTypeEnum.LAKEHOUSE.toString();

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_time")
    private Date createTime;

    /**
     * init > creating > running > created created
     */
    @Column(name = "status")
    private String status;

    @ApiModelProperty("Resource size.")
    @Column(name = "compute_resource_size")
    @Enumerated(EnumType.STRING)
    private ResourceSizeEnum computeResourceSize;

    @ApiModelProperty("Storage resource usage, unit is MB")
    @Column(name = "storage_usage", columnDefinition = "text")
    private String storageUsage;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "release_time")
    private Date releaseTime;

    @Column(name = "create_retries")
    private Integer createRetries;

    @Column(name = "deleted", columnDefinition = "bit(1) default 0")
    private boolean deleted;

    @Column(name = "task_parallelize", columnDefinition = "tinyint(1) default 3")
    private int taskParallelize;

    @Column(name = "version")
    private String version;

    @Column(name = "expire_time")
    private Date expireTime;

    @Column(name = "engine_type")
    private String engineType;
}
