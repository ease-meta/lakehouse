## 前置条件

部署 Lakehouse 需要依赖以下运行环境：

* JDK：[JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html) (1.8+)
* MySQL：[MySQL](https://dev.mysql.com/downloads/mysql/) (5.7+)
* Redis: [Redis](https://redis.io/download/) (5.05+)
* Kubernetes: [Kubernetes](https://kubernetes.io/releases/download/) (1.18.5+)
* 对象存储：可以访问主流的云厂商官网订购相关产品（例如，移动云对象存储 [EOS](https://ecloud.10086.cn/home/product-introduction/eos)）。

## 构建

编译打包：

```bash
$ mvn install -DskipTests -Prelease
```

安装包在 `${LAKEHOUSE_PROJECT_HOME}/lakehouse-api/target` 目录下（`lakehouse-api-${version}-bin.tar.gz`），解压后按需修改部署用户的操作权限。

## 修改配置

基础运行环境准备完毕后，根据具体的环境信息修改配置文件 `${LAKEHOUSE_HOME}/conf/application.yaml`：

```bash
# MySQL的连接信息
spring.datasource.url: jdbc:mysql://localhost:3306/lakehouse?autoReconnect=true&useSSL=false

# 连接MySQL的用户
spring.datasource.username: root

# 连接MySQL的密码
spring.datasource.password: root

# Kubernetes ApiServer的连接信息
spring.kubernetes.api-server: https://localhost:6443

# 连接ApiServer的token信息，参考：https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api
spring.kubernetes.access-token: eJnpc3QiOxTrdPJlcm5ldJKzL3NltnaDImtpZDI6Imotb1lXUnhNeWM3ZRllIiwia3ViPXJuZsRlcy5pRy9zloJ2aW

# 根据需要设置NodeSelector
spring.kubernetes.node-selector: app=lakehouse

# Redis的主机
spring.redis.host: localhost

# Redis的端口
spring.redis.port: 6379

# 连接Redis的密码
spring.redis.password: root

# 对象存储的地址
spring.s3a.endpoint: http://localhost

# 连接对象存储的accessKey
spring.s3a.access-key: 5WSALBJLL3XV6U33DR26

# 连接对象存储的secretKey
spring.s3a.secret-key: JY6ilRHBKk5UR8fDhxAzGBwEjweoEFi56pfwdPME

# 要使用的对象存储的桶
spring.s3a.bucket: test
```

## 初始化数据库

```bash
$ mysql -uroot -p

mysql> set global pxc_strict_mode='DISABLED';

mysql> CREATE DATABASE IF NOT EXISTS lakehouse DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
mysql> CREATE DATABASE IF NOT EXISTS lakehouse_hive_schema DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

# 修改{user}和{password}，与前面配置的MySQL连接信息保持一致
mysql> CREATE USER '{user}'@'%' IDENTIFIED BY '{password}';
mysql> GRANT ALL PRIVILEGES ON `lakehouse%`.* TO '{user}'@'%';
mysql> CREATE USER '{user}'@'localhost' IDENTIFIED BY '{password}';
mysql> GRANT ALL PRIVILEGES ON `lakehouse%`.* TO '{user}'@'localhost';
mysql> FLUSH PRIVILEGES;

mysql> use lakehouse;
mysql> source ${LAKEHOUSE_HOME}/data/lakehouse.sql;
 
mysql> use lakehouse_hive_schema;
mysql> source ${LAKEHOUSE_HOME}/data/lakehouse_hive_schema.sql;
```

## 启停服务

```shell
# 启动
$ sh ./bin/lakehouse.sh start

# 查看状态
$ sh ./bin/lakehouse.sh status

# 停止
$ sh ./bin/lakehouse.sh stop
```

## 登录使用

打开浏览器，访问`http://localhost:19086/lakehouse/ui`登录页面，默认用户以及密码为：`admin/lakehouse@123`。
