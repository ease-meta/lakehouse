## 部署服务

登录到kubernetes api-server节点，执行以下命令：

```bash
# 创建namespace
$ kubectl create ns lakehouse-management

# 将${LAKEHOUSE_HOME}/conf/application.yaml和${LAKEHOUSE_HOME}/data/lakehouse-deployment.yaml拷贝到当面目录，并根据实际的运行环境修改相应配置
$ vi application.yaml

# 创建configmap
$ kubectl create cm api-config --from-file=application.yaml -n lakehouse-management

# 启动deployment
$ kubectl apply -f lakehouse-deployment.yaml -n lakehouse-management

# 修改configmap
$ kubectl edit cm api-config -n lakehouse-management

# 重启deployment
$ kubectl rollout restart api-deployment -n lakehouse-management
```

## 登录使用

打开浏览器，访问`http://localhost:30086/lakehouse/ui`登录页面，默认用户以及密码为：`admin/lakehouse@123`。
