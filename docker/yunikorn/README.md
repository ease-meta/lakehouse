### package

- prerequisite

```
# 1.download and install go env, refer to: https://golang.google.cn/doc/install#download
# 2.change golang proxy to cn

$ go version
$ go env -w GOPROXY=https://goproxy.cn
```

- untar the tarball

```
# download the src tar from: https://yunikorn.apache.org/community/download/

$ tar -zxf apache-yunikorn-0.11.0-incubating-src.tar.gz
$ cd apache-yunikorn-0.11.0-incubating-src
```

- make scheduler and admission image

```
$ cd k8shim

# modify the docker hub url and docker user login as below:
# 1.change "apache" to your dokcer hub repository (e.g. "myhub/myrepo") via 'REGISTRY'
# 2.comment the line which contains "password-stdin"

$ sed -i 's/REGISTRY := apache/REGISTRY := myhub\/myrepo/g' Makefile
$ sed -i 's/^[^#].*password-stdin/#&/g' Makefile

$ make image

# replace by your docker hub password

$ docker login -u ${your_docker_hub_username} -p ${your_docker_hub_password} ${your_docker_hub}
$ make push
```

- make web image

```
$ cd ../web

# modify the docker hub url and docker user login as below:
# 1.change "apache" to your dokcer hub repository (e.g. "myhub/myrepo") via 'REGISTRY'
# 2.comment the line which contains "password-stdin"

$ sed -i 's/REGISTRY := apache/REGISTRY := myhub\/myrepo/g' Makefile
$ sed -i 's/^[^#].*password-stdin/#&/g' Makefile

$ make image

# replace by your docker hub password

$ docker login -u ${your_docker_hub_username} -p ${your_docker_hub_password} ${your_docker_hub}
$ make push_image
```