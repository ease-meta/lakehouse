/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.scheduler;

import com.chinamobile.cmss.lakehouse.api.scheduler.job.ClusterDeployStatusJob;
import com.chinamobile.cmss.lakehouse.api.scheduler.job.ClusterRunningStatusJob;
import com.chinamobile.cmss.lakehouse.api.scheduler.job.EngineTaskMonitorJob;
import com.chinamobile.cmss.lakehouse.api.scheduler.job.EngineTaskRecoverJob;
import com.chinamobile.cmss.lakehouse.api.scheduler.job.EngineTaskSubmitJob;
import com.chinamobile.cmss.lakehouse.service.quartz.QuartzExecutor;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class LakehouseBindOperator {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private QuartzExecutor executor;

    public void schedulerBind() {
        try {
            scheduler.clear();
            // Spark submit
            executor.addJob(EngineTaskSubmitJob.class, "SparkTaskSubmit", "EngineTaskSubmit", "0/10 * * * * ?");
            // Spark monitor
            executor.addJob(EngineTaskMonitorJob.class, "SparkTaskMonitor", "EngineTaskMonitor", "0/5 * * * * ?");
            // Recover task
            executor.addJob(EngineTaskRecoverJob.class, "SparkTaskRecover", "EngineTaskRecover", "0/25 * * * * ?");
            // Cluster deploy status
            executor.addJob(ClusterDeployStatusJob.class, "LakehouseDeployStatus", "ClusterDeployStatus", "0 0/1 * * * ?");
            // Cluster running status
            executor.addJob(ClusterRunningStatusJob.class, "LakehouseRunningStatus", "ClusterRunningStatus", "0 0/10 * * * ?");

            scheduler.start();
        } catch (SchedulerException e) {
            log.error("Failed to submit task scheduling!", e);
        }
    }
}
