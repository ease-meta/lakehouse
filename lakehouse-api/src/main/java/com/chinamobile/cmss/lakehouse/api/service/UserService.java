/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.Map;

public interface UserService {
    /**
     * create user
     *
     * @param loginUser    login user
     * @param userName     user name
     * @param userPassword user password
     * @param description  description
     * @return create result
     */
    Map<String, Object> createUser(UserEntity loginUser, String userName, String userPassword, String description);

    /**
     * update user
     *
     * @param loginUser   login user
     * @param userId      user id
     * @param description description
     * @return update result
     */
    Map<String, Object> updateUser(UserEntity loginUser, String userId, String description);

    /**
     * delete user by id
     *
     * @param loginUser login user
     * @param id        id
     * @return delete result
     */
    Map<String, Object> deleteUserByUserId(UserEntity loginUser, String userId);

    /**
     * create user
     *
     * @param userName     user name
     * @param userPassword user password
     * @param description  description
     * @return create result
     */
    UserEntity createUser(String userName, String userPassword, String description);

    /**
     * get user by user name
     *
     * @param userName user name
     * @return user
     */
    UserEntity getUserByUserName(String userName);

    /**
     * query all general user
     *
     * @param loginUser login user
     * @return general user list
     */
    Map<String, Object> queryAllGeneralUser(UserEntity loginUser);

    /**
     * query user list
     *
     * @param loginUser login user
     * @return user list all
     */
    Map<String, Object> queryUserList(UserEntity loginUser);

    /**
     * query user list page
     *
     * @param loginUser login user
     * @param searchVal search value
     * @param pageNo    page number
     * @param pageSize  page size
     * @return user list page
     */
    Result queryUserList(UserEntity loginUser, String searchVal, Integer pageNo, Integer pageSize);

    /**
     * verify user name
     *
     * @param userName user name
     * @param userId   user id
     * @return true if user name not exists, otherwise return false
     */
    Result verifyUserName(String userName, String userId);

    /**
     * get user info
     *
     * @param loginUser login user
     * @param userId    user id
     * @return user info
     */
    Map<String, Object> getUserInfo(UserEntity loginUser, String userId);

    /**
     * reset password by id
     *
     * @param loginUser    login user
     * @param userId       user id
     * @param userPassword user password
     * @return reset result code
     */
    Map<String, Object> resetPasswordById(UserEntity loginUser, String userId, String userPassword);

    /**
     * get user by user name and password
     *
     * @param userName     user name
     * @param userPassword user password
     * @return user
     */
    UserEntity getUserByUserNameAndPassword(String userName, String userPassword);

    /**
     * find by user id
     *
     * @param userId
     * @return
     */
    UserEntity findByUserId(String userId);
}