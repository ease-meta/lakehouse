/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;
import com.chinamobile.cmss.lakehouse.dao.entity.UserSessionEntity;

public interface UserSessionService {
    /**
     * create user session
     *
     * @param user user
     * @param ip   ip
     * @return session string
     */
    String createUserSession(UserEntity user, String ip);

    /**
     * get user session
     *
     * @param sessionId session id
     * @return user session
     */
    UserSessionEntity getSession(String sessionId);

    /**
     * sign out
     *
     * @param ip        ip
     * @param loginUser login user
     */
    void signOut(String ip, UserEntity loginUser);
}
