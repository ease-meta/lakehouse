/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.controller;

import static com.chinamobile.cmss.lakehouse.common.enums.Status.GET_USER_INFO_ERROR;

import com.chinamobile.cmss.lakehouse.api.security.Authenticator;
import com.chinamobile.cmss.lakehouse.api.service.UserSessionService;
import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.annotation.AccessLogAnnotation;
import com.chinamobile.cmss.lakehouse.common.annotation.ApiException;
import com.chinamobile.cmss.lakehouse.common.enums.Status;
import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api(value = "Login Administration", protocols = "http")
@RestController
@RequestMapping("")
public class UserLoginController extends BaseController {

    @Autowired
    private Authenticator authenticator;

    @Autowired
    private UserSessionService userSessionService;

    /**
     * login
     *
     * @param userName     user name
     * @param userPassword user password
     * @param request      request
     * @param response     response
     * @return login result
     */
    @ApiOperation(value = "login", notes = "LOGIN_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "userName", value = "USER_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "userPassword", value = "USER_PASSWORD", required = true, dataType = "String")
    })
    @PostMapping(value = "/login")
    @ApiException(Status.USER_LOGIN_FAILURE)
    @AccessLogAnnotation(ignoreRequestArgs = {"userPassword", "request", "response"})
    public Result login(@RequestParam(value = "userName") String userName,
                        @RequestParam(value = "userPassword") String userPassword,
                        HttpServletRequest request,
                        HttpServletResponse response) {
        //user name check
        if (StringUtils.isEmpty(userName)) {
            return error(Status.USER_NAME_NULL.getCode(),
                Status.USER_NAME_NULL.getMessage());
        }

        // user ip check
        String ip = getAddress(request);
        if (StringUtils.isEmpty(ip)) {
            return error(Status.IP_IS_EMPTY.getCode(), Status.IP_IS_EMPTY.getMessage());
        }

        // verify username and password
        Result<Map<String, String>> result = authenticator.authenticate(userName, userPassword, ip);
        if (result.getCode() != Status.SUCCESS.getCode()) {
            return result;
        }

        response.setStatus(HttpStatus.SC_OK);
        Map<String, String> cookieMap = result.getData();
        for (Map.Entry<String, String> cookieEntry : cookieMap.entrySet()) {
            Cookie cookie = new Cookie(cookieEntry.getKey(), cookieEntry.getValue());
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
        }
        return result;
    }

    /**
     * sign out
     *
     * @param loginUser login user
     * @param request   request
     * @return sign out result
     */
    @ApiOperation(value = "signOut", notes = "SIGNOUT_NOTES")
    @PostMapping(value = "/signOut")
    @ApiException(Status.SIGN_OUT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = {"loginUser", "request"})
    public Result signOut(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) UserEntity loginUser,
                          HttpServletRequest request) {
        String ip = getAddress(request);
        userSessionService.signOut(ip, loginUser);
        request.removeAttribute(Constants.SESSION_USER);
        return success();
    }

    /**
     * get user info
     *
     * @param loginUser login user
     * @return user info
     */
    @ApiOperation(value = "getCurrentUser", notes = "GET_CURRENT_USER_INFO_NOTES")
    @GetMapping(value = "/get-current-user")
    @ApiException(GET_USER_INFO_ERROR)
    @AccessLogAnnotation
    public Result getCurrentUser(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) UserEntity loginUser) {
        Map<String, Object> result = new HashMap<>();
        result.put(Constants.DATA_LIST, loginUser);
        putMessage(result, Status.SUCCESS);
        return convertToResult(result);
    }
}
