/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.controller;

import static com.chinamobile.cmss.lakehouse.common.Constants.COMMA;
import static com.chinamobile.cmss.lakehouse.common.Constants.HTTP_HEADER_UNKNOWN;
import static com.chinamobile.cmss.lakehouse.common.Constants.HTTP_X_FORWARDED_FOR;
import static com.chinamobile.cmss.lakehouse.common.Constants.HTTP_X_REAL_IP;

import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.enums.Status;
import com.chinamobile.cmss.lakehouse.common.utils.Result;

import java.text.MessageFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public abstract class BaseController {

    public Result checkPageParams(int pageNo, int pageSize) {
        Result result = new Result();
        Status resultEnum = Status.SUCCESS;
        String msg = Status.SUCCESS.getMessage();
        if (pageNo <= 0) {
            resultEnum = Status.REQUEST_PARAMS_NOT_VALID_ERROR;
            msg = MessageFormat.format(Status.REQUEST_PARAMS_NOT_VALID_ERROR.getMessage(), Constants.PAGE_NUMBER);
        } else if (pageSize <= 0) {
            resultEnum = Status.REQUEST_PARAMS_NOT_VALID_ERROR;
            msg = MessageFormat.format(Status.REQUEST_PARAMS_NOT_VALID_ERROR.getMessage(), Constants.PAGE_SIZE);
        }
        result.setCode(resultEnum.getCode());
        result.setMessage(msg);
        return result;
    }

    public Result convertToResult(Map<String, Object> result) {
        Status status = (Status) result.get(Constants.STATUS);
        if (status == Status.SUCCESS) {
            String msg = Status.SUCCESS.getMessage();
            Object datalist = result.get(Constants.DATA_LIST);
            return success(msg, datalist);
        } else {
            Integer code = status.getCode();
            String msg = (String) result.get(Constants.MESSAGE);
            return error(code, msg);
        }
    }

    public Result success() {
        Result result = new Result();
        result.setCode(Status.SUCCESS.getCode());
        result.setMessage(Status.SUCCESS.getMessage());

        return result;
    }

    public Result success(String msg, Object list) {
        return getResult(msg, list);
    }

    public Result error(Integer code, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(msg);
        return result;
    }

    protected void putMessage(Map<String, Object> result, Status status, Object... statusParams) {
        result.put(Constants.STATUS, status);
        if (statusParams != null && statusParams.length > 0) {
            result.put(Constants.MESSAGE, MessageFormat.format(status.getMessage(), statusParams));
        } else {
            result.put(Constants.MESSAGE, status.getMessage());
        }
    }

    private Result getResult(String msg, Object list) {
        Result result = new Result();
        result.setCode(Status.SUCCESS.getCode());
        result.setMessage(msg);

        result.setData(list);
        return result;
    }

    public static String getAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader(HTTP_X_FORWARDED_FOR);

        if (StringUtils.isNotEmpty(ipAddress) && !ipAddress.equalsIgnoreCase(HTTP_HEADER_UNKNOWN)) {
            int index = ipAddress.indexOf(COMMA);
            if (index != -1) {
                return ipAddress.substring(0, index);
            } else {
                return ipAddress;
            }
        }

        ipAddress = request.getHeader(HTTP_X_REAL_IP);
        if (StringUtils.isNotEmpty(ipAddress) && !ipAddress.equalsIgnoreCase(HTTP_HEADER_UNKNOWN)) {
            return ipAddress;
        }

        return request.getRemoteAddr();
    }
}
