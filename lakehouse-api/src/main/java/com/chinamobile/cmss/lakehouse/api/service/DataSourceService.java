/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.api.dto.DataSourceBean;
import com.chinamobile.cmss.lakehouse.common.utils.Result;

import java.util.Map;

public interface DataSourceService {
    /**
     * Create dataSource
     * @param userId
     * @param dataSourceBean
     * @return
     */
    Map<String, Object> create(String userId, DataSourceBean dataSourceBean);

    /**
     * Get dataSource detail
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> get(String userId, Long id);

    /**
     * Update dataSource
     * @param userId
     * @param id
     * @param dataSourceBean
     * @return
     */
    Map<String, Object> update(String userId, Long id, DataSourceBean dataSourceBean);

    /**
     * Soft delete dataSource
     * @param userId
     * @param id
     * @return
     */
    Map<String, Object> delete(String userId, Long id);

    /**
     * Test dataSource connection
     * @param userId
     * @param dataSourceBean
     * @return
     */
    Map<String, Object> testConnection(String userId, DataSourceBean dataSourceBean);

    /**
     * Pageable list dataSources
     * @param userId
     * @param searchVal
     * @param pageNo
     * @param pageSize
     * @return
     */
    Result list(String userId, String searchVal, Integer pageNo, Integer pageSize);
}
