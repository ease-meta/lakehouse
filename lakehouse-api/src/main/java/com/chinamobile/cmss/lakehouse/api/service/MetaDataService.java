/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.common.dto.MetaDataNodeDto;
import com.chinamobile.cmss.lakehouse.common.dto.SearchMetadataTableDto;
import com.chinamobile.cmss.lakehouse.common.dto.SqlExecuteContextDto;
import com.chinamobile.cmss.lakehouse.common.dto.TableNameDto;
import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface MetaDataService {
    Map<String, Object> searchUserTable(UserEntity loginUser, SearchMetadataTableDto searchMetadataTableDto);

    Map<String, Object> getUserDatabases(UserEntity loginUser);

    Map<String, Object> sqlExecuteProxy(UserEntity loginUser, SqlExecuteContextDto sqlExecuteContextDto);

    void deleteTable(UserEntity loginUser, TableNameDto tableNameDto);

    Map<String, List<MetaDataNodeDto>> metadataTree(UserEntity loginUser);

    Result metadataDatabaseTree(UserEntity loginUser);

    Result getTables(UserEntity loginUser, String databaseName);

    Result getColumns(UserEntity loginUser, String databaseName, String tableName);

    boolean checkTableIfExist(UserEntity loginUser, String databaseName, String tableName, boolean checkPermission);
}
