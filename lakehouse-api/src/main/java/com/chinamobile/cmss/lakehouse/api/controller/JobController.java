/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.controller;

import com.chinamobile.cmss.lakehouse.api.dto.JobBean;
import com.chinamobile.cmss.lakehouse.api.service.JobService;
import com.chinamobile.cmss.lakehouse.common.utils.Result;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "Job admin", protocols = "http")
@RestController
public class JobController extends BaseController {
    @Autowired
    private JobService jobService;

    @ApiOperation(value = "View job detail")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/{id}", method = RequestMethod.GET)
    public Result job(@RequestHeader(name = "user_id") String userId,
                      @ApiParam(value = "任务id", required = true) @PathVariable Long id) {
        Map<String, Object> result = jobService.get(userId, id);
        return convertToResult(result);
    }

    @ApiOperation(value = "Create job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job", method = RequestMethod.POST)
    public Result createJob(@RequestHeader(name = "user_id") String userId,
                            @RequestBody JobBean jobBean) {
        Map<String, Object> result = jobService.create(userId, jobBean);
        return convertToResult(result);
    }

    @ApiOperation(value = "Update job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/{id}", method = RequestMethod.PUT)
    public Result updateJob(@RequestHeader(name = "user_id") String userId,
                            @ApiParam(value = "任务id", required = true) @PathVariable Long id,
                            @RequestBody JobBean jobBean) {
        Map<String, Object> result = jobService.update(userId, id, jobBean);
        return convertToResult(result);
    }

    @ApiOperation(value = "Delete job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/{id}", method = RequestMethod.DELETE)
    public Result deleteJob(@RequestHeader(name = "user_id") String userId,
                            @ApiParam(value = "任务id", required = true) @PathVariable Long id) {
        Map<String, Object> result = jobService.delete(userId, id);
        return convertToResult(result);
    }

    @ApiOperation(value = "List job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/list", method = RequestMethod.GET)
    public Result listJob(@RequestHeader(name = "user_id") String userId,
                          @RequestParam("pageNo") Integer pageNo,
                          @RequestParam("pageSize") Integer pageSize,
                          @RequestParam(value = "searchVal", required = false) String searchVal) {
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;

        }
        result = jobService.list(userId, searchVal, pageNo, pageSize);
        return result;
    }

    @ApiOperation(value = "List job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/{id}/scheduleon", method = RequestMethod.GET)
    public Result scheduleOn(@RequestHeader(name = "user_id") String userId,
                             @ApiParam(value = "任务id", required = true) @PathVariable Long id) {
        Map<String, Object> result = jobService.scheduleOn(userId, id);
        return convertToResult(result);
    }

    @ApiOperation(value = "List job")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/job/{id}/scheduleoff", method = RequestMethod.GET)
    public Result scheduleOff(@RequestHeader(name = "user_id") String userId,
                              @ApiParam(value = "任务id", required = true) @PathVariable Long id) {
        Map<String, Object> result = jobService.scheduleOff(userId, id);
        return convertToResult(result);
    }
}
