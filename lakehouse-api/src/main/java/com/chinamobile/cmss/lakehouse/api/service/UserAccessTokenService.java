/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.service;

import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;

import java.util.Map;

public interface UserAccessTokenService {

    /**
     * query user access token list paging
     *
     * @param loginUser login user
     * @param pageNo    page number
     * @param searchVal search value
     * @param pageSize  page size
     * @return token list page
     */
    Result queryUserAccessTokenList(UserEntity loginUser, String searchVal, Integer pageNo, Integer pageSize);

    /**
     * query user access token
     *
     * @param loginUser login user
     * @param userId    user id
     * @return token list
     */
    Map<String, Object> queryUserAccessTokenByUser(UserEntity loginUser, String userId);

    /**
     * create access token
     *
     * @param loginUser  login user
     * @param userId     user id
     * @param expireTime expire time
     * @param token      token
     * @return create access token
     */
    Map<String, Object> createAccessToken(UserEntity loginUser, String userId, String expireTime, String token);


    /**
     * generate token
     *
     * @param loginUser  login user
     * @param userId     user id
     * @param expireTime expire time
     * @return token
     */
    Map<String, Object> generateAccessToken(UserEntity loginUser, String userId, String expireTime);

    /**
     * delete user access token by id
     *
     * @param loginUser login user
     * @param id        token id
     * @return delete token result
     */
    Map<String, Object> deleteUserAccessTokenById(UserEntity loginUser, int id);

    /**
     * update user access token
     *
     * @param loginUser  login user
     * @param id         token id
     * @param userId     user id
     * @param expireTime expire time
     * @param token      token
     * @return updated user access token entity
     */
    Map<String, Object> updateUserAccessToken(UserEntity loginUser, int id, String userId, String expireTime, String token);
}
