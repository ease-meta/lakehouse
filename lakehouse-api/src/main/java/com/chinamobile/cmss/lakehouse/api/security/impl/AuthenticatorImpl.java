/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.security.impl;

import com.chinamobile.cmss.lakehouse.api.security.Authenticator;
import com.chinamobile.cmss.lakehouse.api.service.UserService;
import com.chinamobile.cmss.lakehouse.api.service.UserSessionService;
import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.enums.Status;
import com.chinamobile.cmss.lakehouse.common.utils.Result;
import com.chinamobile.cmss.lakehouse.dao.entity.UserEntity;
import com.chinamobile.cmss.lakehouse.dao.entity.UserSessionEntity;

import java.util.Collections;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthenticatorImpl implements Authenticator {
    @Autowired
    private UserService userService;

    @Autowired
    private UserSessionService userSessionService;

    /**
     * Verifying legality via username and password
     *
     * @param username user name
     * @param password user password
     * @param ip       ip
     * @return result object
     */
    @Override
    public Result<Map<String, String>> authenticate(String username, String password, String ip) {
        Result<Map<String, String>> result = new Result<>();
        UserEntity user = userService.getUserByUserNameAndPassword(username, password);
        if (user == null) {
            result.setCode(Status.USER_NAME_PASSWD_ERROR.getCode());
            result.setMessage(Status.USER_NAME_PASSWD_ERROR.getMessage());
            return result;
        }
        // create session
        String sessionId = userSessionService.createUserSession(user, ip);
        if (sessionId == null) {
            result.setCode(Status.LOGIN_SESSION_FAILED.getCode());
            result.setMessage(Status.LOGIN_SESSION_FAILED.getMessage());
            return result;
        }
        log.info("sessionId : {}", sessionId);
        result.setData(Collections.singletonMap(Constants.SESSION_ID, sessionId));
        result.setCode(Status.SUCCESS.getCode());
        result.setMessage(Status.LOGIN_SUCCESS.getMessage());
        return result;
    }

    /**
     * get authenticated user
     *
     * @param sessionId session id
     * @return user
     */
    @Override
    public UserEntity getAuthUser(String sessionId) {
        UserSessionEntity userSession = userSessionService.getSession(sessionId);
        if (userSession == null) {
            log.info("user session info is null ,session id:{} ", sessionId);
            return null;
        }
        //get user object from session
        return userService.getUserByUserName(userSession.getUserId());
    }
}
