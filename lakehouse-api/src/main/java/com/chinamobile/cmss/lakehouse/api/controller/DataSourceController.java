/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.controller;

import com.chinamobile.cmss.lakehouse.api.dto.DataSourceBean;
import com.chinamobile.cmss.lakehouse.api.service.DataSourceService;
import com.chinamobile.cmss.lakehouse.common.utils.Result;

import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(value = "Data source admin", protocols = "http")
@RestController
public class DataSourceController extends BaseController {

    @Autowired
    private DataSourceService dataSourceService;

    @ApiOperation(value = "Create dataSource")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource", method = RequestMethod.POST)
    public Result createDataSource(@RequestHeader(name = "user_id") String userId,
                                   @RequestBody DataSourceBean dataSourceBean) {
        Map<String, Object> result = dataSourceService.create(userId, dataSourceBean);
        return convertToResult(result);
    }

    @ApiOperation(value = "View dataSource detail")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource/{id}", method = RequestMethod.GET)
    public Result dataSource(@RequestHeader(name = "user_id") String userId,
                             @ApiParam(value = "数据源id", required = true) @PathVariable Long id) {
        Map<String, Object> result = dataSourceService.get(userId, id);
        return convertToResult(result);
    }

    @ApiOperation(value = "Update dataSource")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource/{id}", method = RequestMethod.PUT)
    public Result updateDataSource(@RequestHeader(name = "user_id") String userId,
                                   @ApiParam(value = "数据源id", required = true) @PathVariable Long id,
                                   @RequestBody DataSourceBean dataSourceBean) {
        Map<String, Object> result = dataSourceService.update(userId, id, dataSourceBean);
        return convertToResult(result);
    }

    @ApiOperation(value = "Delete dataSource")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource/{id}", method = RequestMethod.DELETE)
    public Result deleteDataSource(@RequestHeader(name = "user_id") String userId,
                                   @ApiParam(value = "数据源id", required = true) @PathVariable Long id) {
        Map<String, Object> result = dataSourceService.delete(userId, id);
        return convertToResult(result);
    }

    @ApiOperation(value = "Test dataSource")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource/test", method = RequestMethod.POST)
    public Result testDataSource(@RequestHeader(name = "user_id") String userId,
                                 @RequestBody DataSourceBean dataSourceBean) {
        Map<String, Object> result = dataSourceService.testConnection(userId, dataSourceBean);
        return convertToResult(result);
    }

    @ApiOperation(value = "List dataSource")
    @ApiImplicitParams({@ApiImplicitParam(name = "user_id", value = "用户ID", required = true, dataType = "String",
            dataTypeClass = String.class, paramType = "header")})
    @RequestMapping(value = "/datasource/list", method = RequestMethod.GET)
    public Result listDataSource(@RequestHeader(name = "user_id") String userId,
                                 @RequestParam("pageNo") Integer pageNo,
                                 @RequestParam("pageSize") Integer pageSize,
                                 @RequestParam(value = "searchVal", required = false) String searchVal) {
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;

        }
        result = dataSourceService.list(userId, searchVal, pageNo, pageSize);
        return result;
    }
}
