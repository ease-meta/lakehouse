/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.dto;

import com.chinamobile.cmss.lakehouse.common.Constants;
import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@ApiModel
public class LakehouseInstanceBean {

    @ApiModelProperty(value = "instance id, unique", dataType = "String")
    private String instanceId;

    @NotEmpty
    @Pattern(regexp = "^[\\u4e00-\\u9fa5_\\-0-9a-zA-Z]*$", message = "只允许包含中文、大小写字母、数字、“-”、“_”")
    @ApiModelProperty(value = "instance name", dataType = "String", required = true, example = "instanceName")
    private String instance;

    @ApiModelProperty(value = "Cluster Status", dataType = "String")
    private String status;

    @ApiModelProperty(value = "Lakehouse version", dataType = "String", example = "1.0.0")
    private String version;

    @Min(0)
    @ApiModelProperty(value = "max storage, unit: GB", dataType = "Long", required = true, example = "500")
    private Long storageUsage;

    @JsonFormat(pattern = Constants.DATE_FORMAT_PATTERN, timezone = Constants.TIME_ZONE)
    @ApiModelProperty(value = "request time", dataType = "Date", example = "2020-06-30 16:00:00")
    private Date createTime;

    @JsonFormat(pattern = Constants.DATE_FORMAT_PATTERN, timezone = Constants.TIME_ZONE)
    @ApiModelProperty(value = "instance timeout", dataType = "Date", example = "2021-06-30 16:00:00")
    private Date expireTime;

    @NotEmpty
    @ApiModelProperty(value = "creator", dataType = "String", required = true, example = "createUser")
    private String createdBy;

    @ApiModelProperty(value = "computeResourceSize")
    private ResourceSizeEnum computeResourceSize;

    @ApiModelProperty("storageResource")
    private String storageResource;

    @JsonFormat(pattern = Constants.DATE_FORMAT_PATTERN, timezone = Constants.TIME_ZONE)
    @ApiModelProperty(value = "updated time", dataType = "Date", example = "2021-06-30 16:00:00")
    private Date updateTime;

    @Min(0)
    @ApiModelProperty(value = "task running time (s)", dataType = "Long", required = true, example = "600")
    private Long taskRunningTime;

    @ApiModelProperty("user name")
    private String userName;

    @ApiModelProperty("taskParallelize")
    private int taskParallelize;

    @ApiModelProperty(value = "engineType", dataType = "String", example = "Spark")
    private String engineType;

    @ApiModelProperty(value = "clusterType", dataType = "String", example = "Lakehouse")
    private String clusterType;
}
