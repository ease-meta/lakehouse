/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.api.dto;

import com.chinamobile.cmss.lakehouse.common.enums.DataSourceTypeEnum;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "数据连接-创建")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataSourceBean {
    private Long id;

    @ApiModelProperty(value = "数据连接名称：长度为4~32位, 仅包含字母、数字、中划线或下划线, 必须以字母开头", required = true, example = "datasource-01")
    @NotBlank(message = "数据连接名称不能为空")
    @Size(max = 32, message = "连接名长度为4~32个字符")
    private String name;

    @ApiModelProperty(value = "数据连接url: jdbc:mysql://127.0.0.1:3306", example = "jdbc:mysql://127.0.0.1:3306")
    private String url;

    @ApiModelProperty(value = "数据连接分类,默认值:MySQL", required = true, example = "MYSQL")
    @Enumerated(EnumType.STRING)
    private DataSourceTypeEnum dataSourceType;

    @ApiModelProperty(value = "数据库服务器ip", example = "127.0.0.1")
    private String ip;

    @ApiModelProperty(value = "端口", example = "3306")
    private Integer port;

    @ApiModelProperty(value = "数据连接的用户名", example = "lakehouse")
    private String username;

    @ApiModelProperty(value = "密码(AES加密密文形式)，密码不能含有空白字符", example = "lakehouse")
    @Size(max = 1024, message = "密码明文加密后超过了1024位")
    private String password;

    @ApiModelProperty("创建人ID")
    private String createUserId;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新人ID")
    private String updateUserId;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("是否软删除，0：正常，1：软删除")
    private Boolean deleted = Boolean.FALSE;
}
