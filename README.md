# Lakehouse

云原生大数据分析 Lakehouse 是一个兼容主流云厂商对象存储的一站式 Serverless 融合大数据处理分析服务，帮助用户快速进行数据湖探索，洞察业务价值。

## Features

* 兼容主流云厂商的对象存储：可直接基于原有对象存储快速搭建数据湖平台
* All in SQL 开发：提供可视化交互式 SQL 的数据开发平台，提升数据开发效率
* 跨源数据分析：支持跨源分析，简化对接多种数据源进行探索的工作流程

## Overview

![overview-architecture](docs/images/overview-arch.png)

## Getting Started

* [编译和部署](docs/deploy-bare-metal.md)

## Contributing

如果您有好的建议和想法，欢迎提 Issues 或 Pull Requests，为共建社区贡献自己的力量。基本流程和代码规范请参考 [CONTRIBUTING](CONTRIBUTING.md)。

## License

详情可参考 [LICENSE](LICENSE)。
