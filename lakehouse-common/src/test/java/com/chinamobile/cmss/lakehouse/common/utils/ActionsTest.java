/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class ActionsTest {

    private final int numRetries = 5;
    private final long sleepBetweenRetriesMs = 500;

    @Test
    public void testRun() {
        Actions.Action testAction =
            Actions.Action.builder().actionName("testAction").numRetries(numRetries)
                .sleepBetweenInvocationsMs(sleepBetweenRetriesMs).supplier(() -> {
                    log.info("test Action builder");
                    return Actions.ActionResult.builder().success(true).build();
                }).build();

        AtomicBoolean success = new AtomicBoolean(false);
        Actions.newBuilder()
            .addAction(testAction.toBuilder().onSuccess((ignored) -> success.set(true)).build()).run();
        if (!success.get()) {
            throw new RuntimeException("Failed to test action build %s");
        }
    }

    @Test
    public void testNumbers() {
        Actions actions = Actions.newBuilder();
        Actions.Action testAction =
            Actions.Action.builder().actionName("testAction").numRetries(numRetries)
                .sleepBetweenInvocationsMs(sleepBetweenRetriesMs).supplier(() -> {
                    log.info("test Action builder");
                    return Actions.ActionResult.builder().success(true).build();
                }).build();

        AtomicBoolean success = new AtomicBoolean(false);
        actions.addAction(testAction.toBuilder().onSuccess((ignored) -> success.set(true)).build())
            .run();
        if (!success.get()) {
            throw new RuntimeException("Failed to test action build %s");
        }

        Actions.Action testAction2 =
            Actions.Action.builder().actionName("testAction2").numRetries(numRetries)
                .sleepBetweenInvocationsMs(sleepBetweenRetriesMs).supplier(() -> {
                    log.info("test Action2 builder");
                    return Actions.ActionResult.builder().success(true).build();
                }).build();

        actions.addAction(testAction2);

        assertEquals(actions.numActions(), 2);

    }
}
