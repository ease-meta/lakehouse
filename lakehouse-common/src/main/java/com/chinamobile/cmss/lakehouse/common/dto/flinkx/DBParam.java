/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto.flinkx;

import com.chinamobile.cmss.lakehouse.common.enums.DBTableType;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "数据同步任务-结构化DB类型参数(MySQL,SqlServer,PostgreSQL)")
public class DBParam implements Serializable {
    private static final long serialVersionUID = 550182908272051688L;

    @ApiModelProperty("数据库名称")
    private String dataBase;

    @ApiModelProperty("迁移表类型，SELECTED_TABLES(0)：指定表，ALL_TABLES(1)：全部表")
    private DBTableType dbTableType;

    @ApiModelProperty("指定表，列表模式。当DBTableType为SELECTED_TABLES时，tables不能为空")
    private List<String> tables;
}
