/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import lombok.Getter;

@Getter
public enum ResourceSizeEnum {
    /**
     * small type
     */
    SMALL("small", "小型"),
    /**
     * medium type
     */
    MEDIUM("medium", "中型"),
    /**
     * large type
     */
    LARGE("large", "大型"),
    /**
     * xlarge type
     */
    XLARGE("xlarge", "X大型");

    private String description;
    private String type;

    ResourceSizeEnum(String type, String description) {
        this.description = description;
        this.type = type;
    }
}
