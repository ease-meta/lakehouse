/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

import com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant;

import java.math.BigDecimal;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.custom.QuantityFormatter;
import io.kubernetes.client.openapi.models.V1ResourceRequirements;

public class ResourceUtils {

    private static Quantity cpu(double cpu) {
        return new Quantity(new BigDecimal(cpu), Quantity.Format.DECIMAL_SI);
    }

    private static Quantity memory(double memoryOfGb) {
        return new QuantityFormatter().parse(memoryOfGb + K8sModelConstant.GIB);
    }

    public static V1ResourceRequirements resourceRequirements(double cpuOverScore, double vcores,
                                                              double memoryOfGb) {
        Map<String, Quantity> requests = ImmutableMap.of(K8sModelConstant.CPU_RES,
            cpu(vcores / cpuOverScore), K8sModelConstant.MEMORY_RES, memory(memoryOfGb));
        Map<String, Quantity> limits = ImmutableMap.of(K8sModelConstant.CPU_RES, cpu(vcores),
            K8sModelConstant.MEMORY_RES, memory(memoryOfGb));
        return new V1ResourceRequirements().requests(requests).limits(limits);
    }

}
