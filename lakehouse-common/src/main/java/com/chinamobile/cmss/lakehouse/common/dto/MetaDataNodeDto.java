/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class MetaDataNodeDto {

    private static Random random = new Random();

    private String label;

    private String key;

    private ScopedSlots scopedSlots;

    private List<MetaDataNodeDto> children;

    // database、table not leaf, column is leaf
    private Boolean isLeaf;

    @Data
    public static class ScopedSlots {
        private TitleType title;
        private String parentLabel;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @SuppressWarnings("PMD.EnumConstantsMustHaveCommentRule")
    public enum TitleType {
        database,
        table,
        column
    }

    public static MetaDataNodeDto create(String label, TitleType titleType, Boolean isLeaf, String parentLabel) {
        MetaDataNodeDto metaDataNodeDto = new MetaDataNodeDto();
        metaDataNodeDto.setLabel(label);
        ScopedSlots scopedSlots = new ScopedSlots();
        scopedSlots.setTitle(titleType);
        scopedSlots.setParentLabel(parentLabel);
        metaDataNodeDto.setScopedSlots(scopedSlots);
        metaDataNodeDto.setKey(random.nextInt(Integer.MAX_VALUE) + "");
        metaDataNodeDto.setIsLeaf(isLeaf);
        return metaDataNodeDto;
    }

}
