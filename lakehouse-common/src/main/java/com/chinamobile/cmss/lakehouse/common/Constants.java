/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common;

import java.util.regex.Pattern;

public class Constants {

    private Constants() {
        throw new UnsupportedOperationException("Construct Constants");
    }

    /**
     * common properties path
     */
    public static final String COMMON_PROPERTIES_PATH = "/common.properties";

    /**
     * hive properties path
     */
    public static final String HIVE_SERVER_SITE_PATH = "/xml/hive-site.xml";
    public static final String HIVE_META_SITE_PATH = "/xml/meta-site.xml";
    public static final String HIVE_DRIVER_NAME = "hive.driver.name";
    public static final String HIVE_MAX_TOTAL_PER_KEY = "hive.pool.config.max.total.per.key";
    public static final String HIVE_MAX_IDLE_PER_KEY = "hive.pool.config.max.idle.per.key";
    public static final String HIVE_EVICTOR_SHUTDOWN_TIMEOUT = "hive.pool.config.evictor.shutdown.timeout";
    public static final String HIVE_MIN_EVICTABLE_IDLE_TIME = "hive.pool.config.min.evictable.idle.time";
    public static final String HIVE_MAX_WAIT = "hive.pool.config.max.wait";
    public static final String HIVE_TIME_BETWEEN_EVICTION_RUNS = "hive.pool.config.time.between.eviction.runs";

    /**
     * template yaml path
     */
    public static final String TEMPLATE_HIVE_YAML_PATH = "/yaml/hive.yaml";
    public static final String TEMPLATE_META_YAML_PATH = "/yaml/meta.yaml";
    public static final String TEMPLATE_YUNIKORN_YAML_PATH = "/yaml/yunikorn.yaml";

    /**
     * kubernetes image configs
     */
    public static final String KUBERNETES_IMAGE_HIVE = "kubernetes.image.hive";
    public static final String KUBERNETES_IMAGE_SPARK = "kubernetes.image.spark";
    public static final String KUBERNETES_IMAGE_YUNIKORN_SCHEDULER = "kubernetes.image.yunikorn.scheduler";
    public static final String KUBERNETES_IMAGE_YUNIKORN_WEB = "kubernetes.image.yunikorn.web";
    /**
     * kubernetes resource configs
     */
    public static final String KUBERNETES_RESOURCE_REPLICAS_HIVE = "kubernetes.resource.replicas.hive";
    public static final String KUBERNETES_RESOURCE_REPLICAS_META = "kubernetes.resource.replicas.meta";
    public static final String KUBERNETES_RESOURCE_REPLICAS_YUNIKORN = "kubernetes.resource.replicas.yunikorn";
    public static final String KUBERNETES_RESOURCE_CONTAINER_VCORE = "kubernetes.resource.container.vcore";
    public static final String KUBERNETES_RESOURCE_CONTAINER_HEAP_HIVE = "kubernetes.resource.container.heap.hive";
    public static final String KUBERNETES_RESOURCE_CONTAINER_HEAP_META = "kubernetes.resource.container.heap.meta";
    public static final String KUBERNETES_RESOURCE_CONTAINER_HEAP_YUNIKORN = "kubernetes.resource.container.heap.yunikorn";
    public static final String KUBERNETES_RESOURCE_EXPOSE_SERVICE_ENABLED = "kubernetes.resource.expose.service.enabled";

    /**
     * kubernetes informer client configs
     */
    public static final String KUBERNETES_INFORMER_WAIT_MS = "kubernetes.informer.wait.ms";
    public static final String KUBERNETES_INFORMER_SYNC_RETRIES_NUMBER = "kubernetes.informer.sync.retries.number";
    public static final String KUBERNETES_INFORMER_SYNC_RETRIES_INTERVAL_MS = "kubernetes.informer.sync.retries.interval.ms";
    public static final String KUBERNETES_INFORMER_CHECK_RETRIES_NUMBER = "kubernetes.informer.check.retries.number";
    public static final String KUBERNETES_INFORMER_CHECK_RETRIES_INTERVAL_MS = "kubernetes.informer.check.retries.interval.ms";

    /**
     * kubernetes ratio configs
     */
    public static final String KUBERNETES_RATIO_CPU_OVER = "kubernetes.ratio.cpu.over";
    public static final String KUBERNETES_RATIO_MEMORY_OVER = "kubernetes.ratio.memory.over";

    /**
     * resource configs
     */
    public static final String RESOURCE_SIZE_SMALL = "resource.size.small";
    public static final String RESOURCE_SIZE_MEDIUM = "resource.size.medium";
    public static final String RESOURCE_SIZE_LARGE = "resource.size.large";
    public static final String RESOURCE_SIZE_XLARGE = "resource.size.xlarge";
    public static final String RESOURCE_STRATEGY = "resource.strategy";
    public static final String RESOURCE_MANAGER = "resource.manager";
    public static final String RESOURCE_SCALE = "resource.scale";

    /**
     * date format pattern
     */
    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_ZONE = "GMT+8";

    /**
     * common symbols
     */
    public static final String COMMA = ",";
    public static final String COLON = ":";
    public static final String QUESTION = "?";
    public static final String SPACE = " ";
    public static final String SINGLE_SLASH = "/";
    public static final String DOUBLE_SLASH = "//";
    public static final String EQUAL_SIGN = "=";
    public static final String AT_SIGN = "@";

    /**
     * http connect settings
     */
    public static final int HTTP_CONNECT_TIMEOUT = 60 * 1000;
    public static final int HTTP_CONNECTION_REQUEST_TIMEOUT = 60 * 1000;
    public static final int SOCKET_TIMEOUT = 60 * 1000;
    public static final String HTTP_HEADER_UNKNOWN = "unKnown";
    public static final String HTTP_X_FORWARDED_FOR = "X-Forwarded-For";
    public static final String HTTP_X_REAL_IP = "X-Real-IP";
    public static final String USER_ID = "userId";

    /**
     * http response settings
     */
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String COUNT = "count";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PAGE_NUMBER = "pageNo";
    public static final String DATA_LIST = "data";
    public static final String TOTAL_LIST = "totalList";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String TOTAL_PAGE = "totalPage";
    public static final String TOTAL = "total";

    /**
     * session settings
     */
    public static final String SESSION_USER = "session.user";
    public static final String SESSION_ID = "sessionId";

    /**
     * user name regex
     */
    public static final Pattern USER_NAME_PATTERN = Pattern.compile("^[a-zA-Z0-9_-]{2,32}$");
    /**
     * user password regex
     */
    public static final Pattern USER_PASSWORD_PATTERN = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&$^()*=_+-])[A-Za-z0-9!@#%&$^()*=_+-]{8,20}");

    /**
     * engine settings
     */
    public static final String ENGINE_SPARK_HOME = "engine.spark.home";
    public static final String ENGINE_SPARK_WORKER_LOG_DIR = "engine.spark.worker.log.dir";
    public static final String ENGINE_SQL_APP_MAIN_CLASS = "engine.sql.app.main.class";
    public static final String ENGINE_SQL_APP_RESOURCE = "engine.sql.app.resource";
    public static final String ENGINE_AWS_PATH_STYLE_ACCESS = "engine.aws.path.style.access";

    /**
     * fs.s3
     */
    public static final String S3A_ACCESS_KEY = "fs.s3a.access.key";
    public static final String S3A_ENDPOINT = "fs.s3a.endpoint";
    public static final String S3A_SECRET_KEY = "fs.s3a.secret.key";

    /**
     * session timeout
     */
    public static final int SESSION_TIME_OUT = 7200;

    /**
     * Sync job config
     */
    public static final String SYNC_JOB_PREFIX = "sync";
}
