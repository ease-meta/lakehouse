/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import com.chinamobile.cmss.lakehouse.common.enums.ClusterTypeEnum;
import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class LakehouseCreateRequest {
    private String instanceId;
    private ResourceSizeEnum resouceSize;
    private ClusterTypeEnum type;
    private String createUser;
    private String instance;
    private String engineType;

    public LakehouseCreateRequest(String instanceId) {
        this.instanceId = instanceId;
        this.resouceSize = ResourceSizeEnum.SMALL;
        this.type = ClusterTypeEnum.LAKEHOUSE;
    }

    public LakehouseCreateRequest() {

    }

}
