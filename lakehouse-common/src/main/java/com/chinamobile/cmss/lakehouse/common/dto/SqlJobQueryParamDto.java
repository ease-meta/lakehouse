/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import com.chinamobile.cmss.lakehouse.common.enums.TaskStatusTypeEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class SqlJobQueryParamDto {

    /**
     * user Id
     */
    @ApiModelProperty(value = "user id")
    private String userId;

    @ApiModelProperty(value = "task id", required = true)
    private String taskId;

    @ApiModelProperty(value = "engine type", required = true)
    private String engineType;

    @ApiModelProperty(value = "task name")
    private String name;

    @ApiModelProperty(value = "hive db name", required = true)
    private String dbName;

    @ApiModelProperty(value = "task type", required = true)
    private String taskType;

    @ApiModelProperty(value = "task status")
    private List<TaskStatusTypeEnum> status = new ArrayList<>();

    @ApiModelProperty(value = "instance")
    private String instance;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "start time")
    private Date submitTimeFrom;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "end time")
    private Date submitTimeTo;

    @ApiModelProperty(value = "page info")
    private PageBean pageDto;

    @ApiModelProperty(value = "sortField")
    private String sortField;

    @ApiModelProperty(value = "sortOrder")
    private String sortOrder;

    @ApiModelProperty(value = "scheduleModel")
    private String scheduleModel;

    @ApiModelProperty(value = "related task id")
    private String relatedTaskId;

    @ApiModelProperty(value = "related task name")
    private String relatedTaskName;
}
