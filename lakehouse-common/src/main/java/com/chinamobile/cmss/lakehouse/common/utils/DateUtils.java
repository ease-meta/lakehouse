/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

import com.chinamobile.cmss.lakehouse.common.Constants;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@Slf4j
public final class DateUtils {

    private static final DateTimeFormatter YYYY_MM_DD_HH_MM_SS = DateTimeFormat.forPattern(Constants.DATE_FORMAT_PATTERN);

    /**
     * convert date str to yyyy-MM-dd HH:mm:ss format
     *
     * @param dateStr date string
     * @return yyyy-MM-dd HH:mm:ss format
     */
    public static Date stringToDate(String dateStr) {
        return YYYY_MM_DD_HH_MM_SS.parseDateTime(dateStr).toDate();
    }
}
