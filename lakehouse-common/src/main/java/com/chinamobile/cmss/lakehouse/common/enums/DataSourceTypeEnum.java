/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DataSourceTypeEnum {
    /**
     * MySQL DataSource
     */
    MYSQL("db", "MySQL", "com.mysql.jdbc.Driver"),

    /**
     * S3/OSS
     */
    S3("file", "S3", ""),

    /**
     * Hive
     */
    HIVE("nosql", "Hive", "org.apache.hive.jdbc.HiveDriver"),
    ;

    private final String dsRootType;
    private final String dsType;
    private final String driverName;

    /**
     * The init dataSource nodeId
     *
     * @param nodeId
     * @return
     */
    public static DataSourceTypeEnum convert(int nodeId) {
        DataSourceTypeEnum dataSourceType;
        switch (nodeId) {
            case 300:
                dataSourceType = MYSQL;
                break;
            case 120:
                dataSourceType = S3;
                break;
            default:
                dataSourceType = null;
        }
        Objects.requireNonNull(dataSourceType, "could not find valid type for nodeId " + nodeId);
        return dataSourceType;
    }

    public static DataSourceTypeEnum stringToEnum(String nodeName) {
        String nodeNameLower = nodeName.toLowerCase();
        DataSourceTypeEnum dataSourceType;
        switch (nodeNameLower) {
            case "mysql":
                dataSourceType = MYSQL;
                break;
            case "s3":
                dataSourceType = S3;
                break;
            default:
                dataSourceType = null;
        }
        Objects.requireNonNull(dataSourceType, "could not find valid type for nodeName " + nodeName);
        return dataSourceType;
    }
}
