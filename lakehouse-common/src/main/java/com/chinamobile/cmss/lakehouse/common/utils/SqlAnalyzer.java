/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.hive.visitor.HiveSchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.alibaba.druid.util.JdbcConstants;

public class SqlAnalyzer {
    public static List<String> getDatabaseAndTables(String sqlContext) {
        List<TableStat.Name> result = new ArrayList<>();
        List<SQLStatement> stmtList = SQLUtils.parseStatements(sqlContext, JdbcConstants.HIVE);
        for (SQLStatement stm : stmtList) {
            HiveSchemaStatVisitor visitor = new HiveSchemaStatVisitor();
            stm.accept(visitor);
            result.addAll(visitor.getTables().keySet());
        }
        return result.stream().map(TableStat.Name::getName).collect(Collectors.toList());
    }
}
