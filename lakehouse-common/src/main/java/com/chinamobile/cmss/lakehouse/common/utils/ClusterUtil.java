/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils;

public class ClusterUtil {

    public static final String CLUSTER_NAME_WITH_PREFIX = "lakehouse-";

    /**
     * convert cluster name to namespace
     *
     * @param clusterName cluster name
     * @return
     */
    public static String convertCluster2Namespace(String clusterName) {
        return String.format("%s%s", CLUSTER_NAME_WITH_PREFIX, clusterName.replace('_', '-'));
    }

    /**
     * convert cluster name to namespace, example: lakehouse-test-1 -> test_1
     *
     * @param namespace the namespace
     * @return
     */
    public static String convertNamespace2ClusterName(String namespace) {
        return namespace.replace(CLUSTER_NAME_WITH_PREFIX, "").replace('-', '_');
    }
}
