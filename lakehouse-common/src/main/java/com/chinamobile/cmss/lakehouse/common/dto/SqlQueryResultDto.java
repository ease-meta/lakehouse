/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import com.chinamobile.cmss.lakehouse.common.enums.TaskStatusTypeEnum;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class SqlQueryResultDto {
    /**
     * Execution status
     */
    TaskStatusTypeEnum status;

    /**
     * Execution error msg
     */
    String errorMsg;

    /**
     * Execution error msg
     */
    String syncErrorMsg;

    /**
     * sql will effect table, such as `create drop sql`
     */
    @JsonIgnore
    Boolean tableAffectedSql;

    /**
     * result column list
     */
    List<String> columnList;


    /**
     * origin column list
     */
    List<Map<String, String>> originColumnList;

    /**
     * result set, one to one correspondence with column
     */
    List<List<String>> resultSet;

    /**
     * execute time
     */
    String executeTime;

    /**
     * select sql's total count
     */
    String totalDataCount;


    /**
     * none select sql execution affect rows count
     */
    String affectedCount;

    /**
     * for frontend
     */
    String affectedRows;

    /**
     * total column count
     */
    String totalColumnCount;

    /**
     * sql
     */
    String querySql;

    /**
     * user id
     */
    String userId;

}
