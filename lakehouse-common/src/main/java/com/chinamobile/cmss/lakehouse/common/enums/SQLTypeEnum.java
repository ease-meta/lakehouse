/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

public enum SQLTypeEnum {

    ALTER("alter", "alter table or partition statement, hive"),
    CREATE("create", "create table statement, hive"),
    DESCRIBE("describe", "get table or partition information statement, hive"),
    DESC("desc", "get table or partition information statement, hive"),
    DROP("drop", "drop table or partition statement, hive"),
    FROM("from", "from prepositional statement, spark"),
    GRANT("grant", "grant statement, hive"),
    INSERT("insert", "insert statement, spark"),
    MSCK("msck", "修复分区语句, spark"),
    REVOKE("revoke", "permission recycling statement, hive"),
    SELECT("select", "query statement, spark"),
    SHOW("show", "show table or partition statement, hive"),
    WITH("with", "with .. as statement, spark"),
    ANALYZE("analyze", "analyze statement, spark"),
    TRUNCATE("truncate", "truncate statement, spark");

    String status;
    String description;

    SQLTypeEnum(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }
}