/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.kubernetes;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public class K8sModelConstant {

    public static final String VALID_K8S_NAME_REGEX =
        "[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*";

    public static final String STATEFUL_SET_HOSTNAME_SEPARATOR = "-";

    public static final String STATEFUL_SET_DOMAIN_NAME_SEPARATOR = ".";

    public static final String HOST_PORT_SEPARATOR = ":";

    public static final String API_VERSION = "v1";

    public static final String APPS_API_VERSION = "apps/v1";

    public static final String NETWORKING_API_VERSION = "networking.k8s.io/v1";

    public static final String RBAC_API_VERSION = "rbac.authorization.k8s.io/v1";

    public static final String RBAC_API_GROUP = "rbac.authorization.k8s.io";

    public static final String JMX_EXPORTER_NAME = "jmxexporter";

    public static final Integer JMX_EXPORTER_PORT = 7000;

    public static final String NODE_CPU = "cpu";
    public static final String NODE_MEMORY = "memory";

    /**
     * Calculate pod limits cpu and memory with status phase: Running, Pending refer to:
     * https://kubernetes.io/zh/docs/concepts/workloads/pods/pod-lifecycle/
     */
    public static final Set<String> POD_PHASE = ImmutableSet.of("Running", "Pending");

    /**
     * Image pull policy. One of Always, Never, IfNotPresent. Defaults to Always if :latest tag is
     * specified, or IfNotPresent otherwise. Cannot be updated. More info:
     * https://kubernetes.io/docs/concepts/containers/images#updating-images
     **/
    public static final String IMAGE_PULL_POLICY = "Always";

    public static final String STATEFUL_SET_KIND = "StatefulSet";

    public static final String DEPLOYMENT_KIND = "Deployment";

    public static final String SERVICE_KIND = "Service";

    public static final String NAMESPACE_KIND = "Namespace";

    public static final String CLUSTER_ROLE_BINDING_KIND = "ClusterRoleBinding";

    public static final String CLUSTER_ROLE_KIND = "ClusterRole";

    public static final String SUBJECT_KIND = "Group";

    public static final String POD_KIND = "Pod";

    public static final String CONFIG_MAP_KIND = "ConfigMap";

    public static final String JOB_KIND = "Job";

    public static final String NETWORK_POLICY_KIND = "NetworkPolicy";

    public static final String PERSISTENT_VOLUME_KIND = "PersistentVolume";

    public static final String PERSISTENT_VOLUME_CLAIM_KIND = "PersistentVolumeClaim";

    public static final String NONE_CLUSTER_IP = "None";

    public static final String NODE_PORT = "NodePort";

    public static final String STATEFUL_SET_UPDATE_STRATEGY_TYPE = "RollingUpdate";

    public static final String JVM_HEAP_SIZE_HOLDER = "JVM_HEAP_SIZE_HOLDER";

    public static final String POD_MANAGEMENT_POLICY = "OrderedReady";

    public static final String CPU_RES = "cpu";

    public static final String MEMORY_RES = "memory";

    public static final String GIB = "Gi";

    public static final String UNIX_PATH_SEPARATOR = "/";

    public static final String NAMESPACE_SEPARATOR = "-";

    public static final String CLUSTER_NAMESPACE_PREFIX = "lakehouse";

    public static final String COMMON_NAMESPACE_SUFFIX = "system";

    public static final String DEFAULT_NAMESPACE = "default";

    /**
     * min valid port
     */
    public static final int MIN_PORT = 1000;
    /**
     * max valid port
     */
    public static final int MAX_PORT = 65535;

    /**
     * labels
     */
    public static final String LABEL_APP = "app";
    public static final String LABEL_APP_LAKEHOUSE = "lakehouse";
    public static final String LABEL_APP_YUNIKORN = "yunikorn";

    public static final String LABEL_COMPONENT = "component";
    public static final String LABEL_COMPONENT_PORT_DISCOVER = "discover";
    public static final String LABEL_COMPONENT_HIVE = "hive";
    public static final String LABEL_COMPONENT_META = "meta";
    public static final String LABEL_COMPONENT_YUNIKORN = "yunikorn";
    public static final String LABEL_COMPONENT_POD_NAME_LABEL = "statefulset.kubernetes.io/pod-name";

    public static final Map<String, String> DISCOVER_SERVICE_LABELS = ImmutableMap.of(LABEL_APP,
        LABEL_APP_LAKEHOUSE, LABEL_COMPONENT, LABEL_COMPONENT_PORT_DISCOVER);

    public static final Map<String, String> HIVE_SERVICE_LABELS =
        ImmutableMap.of(LABEL_APP, LABEL_APP_LAKEHOUSE, LABEL_COMPONENT, LABEL_COMPONENT_HIVE);

    public static final Map<String, String> META_SERVICE_LABELS =
        ImmutableMap.of(LABEL_APP, LABEL_APP_LAKEHOUSE, LABEL_COMPONENT, LABEL_COMPONENT_META);

    public static final Map<String, String> YUNIKORN_SERVICE_LABELS =
        ImmutableMap.of(LABEL_APP, LABEL_APP_LAKEHOUSE, LABEL_COMPONENT, LABEL_COMPONENT_YUNIKORN);

    public static final Map<String, String> YUNIKORN_CONFIG_MAP_LABELS =
        ImmutableMap.of(LABEL_APP, LABEL_APP_YUNIKORN);

    public static final String SERVICE_NAME_DISCOVER =
        LABEL_APP_LAKEHOUSE + "-" + LABEL_COMPONENT_PORT_DISCOVER;

    public static final Map<String, String> NAMESPACE_LABELS =
        ImmutableMap.of(LABEL_APP, LABEL_APP_LAKEHOUSE);

    public static final String LABEL_SELECTOR =
        K8sModelConstant.LABEL_APP + "=" + K8sModelConstant.LABEL_APP_LAKEHOUSE;

    public static final String LABEL_SELECTOR_YUNIKORN =
        K8sModelConstant.LABEL_APP + "=" + K8sModelConstant.LABEL_APP_YUNIKORN;
}
