/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto.flinkx;

import java.io.Serializable;

import lombok.Data;

@Data
public class FieldConf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 字段名称 */
    private String name;
    /** 字段类型 */
    private String type;
    /** 字段索引 */
    private Integer index;
    /** 字段常量值 */
    private String value;
    /** 如果字段是时间字符串，可以指定时间的格式，将字段类型转为日期格式返回 */
    private String format;
    /** 与format字段组合使用，用于时间字符串格式转换 parseFormat->时间戳->format */
    private String parseFormat;
    /** 字段分隔符 */
    private String splitter;
    /** 是否为分区字段 */
    private Boolean isPart = false;
    /** 字段是否可空 */
    private Boolean notNull = false;
    /** 字段长度 */
    private Integer length;
}
