/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.utils.spark

import com.chinamobile.cmss.lakehouse.common.utils.KeyValue

import java.util

object SparkSqlTblInfo {

  def getTables(sql: String, clusterName: String): util.ArrayList[KeyValue[String, String]] = {
    val tableRefs = SparkSqlAnalyze.filterTables(sql)
    val tblList = new util.ArrayList[KeyValue[String, String]]

    tableRefs.foreach { f =>
      f.database match {
        case Some(db) =>
          tblList.add(KeyValue.of(db, f.table))

        case None =>
          tblList.add(KeyValue.of(clusterName, f.table))
      }
    }
    tblList
  }
}
