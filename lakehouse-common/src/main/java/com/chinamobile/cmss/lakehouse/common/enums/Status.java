/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import java.util.Locale;
import java.util.Optional;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * status enum
 */
public enum Status {

    /**
     * success
     */
    SUCCESS(0, "success", "成功"),

    INTERNAL_SERVER_ERROR_ARGS(10000, "Internal Server Error: {0}", "服务端异常: {0}"),

    REQUEST_PARAMS_NOT_VALID_ERROR(10001, "request parameter {0} is not valid", "请求参数[{0}]无效"),
    LAKEHOUSE_RESOURCE_CREATE_ERROR(10002, "lakehouse resource create error", "lakehouse资源创建错误"),
    LAKEHOUSE_RESOURCE_RELEASE_ERROR(10003, "lakehouse resource release error", "lakehouse资源释放错误"),
    LAKEHOUSE_EXIST(10004, "lakehouse instance already exists", "lakehouse实例已存在"),
    LAKEHOUSE_INSTANCE_NAME_LIST_ERROR(10005, "lakehouse fetch instance list error", "lakehouse 返回实例列表错误"),
    LAKEHOUSE_INSTANCE_LIST_NULL_ERROR(10005, "can not find instance", "lakehouse 实例为空"),
    LAKEHOUSE_INSTANCE_FROZEN_ERROR(10005, "instance status is frozen", "lakehouse 实例被冻结"),

    SQL_CONSOLE_FORMAT_ERROR(10101, "format sql error", "格式化 sql 失败"),
    SQL_CONSOLE_SQL_NULL_ERROR(10102, "format error, sql is null", "sql 格式化失败 ,原因：sql 不能为空"),
    SQL_CONSOLE_NO_HIVE_PERMISSION_ERROR(10103, "format error, user does not have permission to operate hive library", "sql 格式化失败 ,原因：用户没有操作 hive 库的权限！"),
    SQL_CONSOLE_SQL_DECODE_ERROR(10104, "sql decode failed: {0}", "sql 解码失败: [{0}]"),
    SQL_CONSOLE_EXECUTE_ERROR(10105, "execute sql error", "执行 sql 失败"),
    SQL_CONSOLE_SQL_COMMIT_FAILED_ERROR(10105, "commit sql job failed", "sql 提交任务失败"),
    SQL_CONSOLE_SQL_ABORT_ERROR(10105, "abort job error", "终止作业失败"),
    SQL_CONSOLE_TASK_NOT_FOUND_ERROR(10106, "task not found", "作业不存在"),
    SQL_CONSOLE_SQL_QUERY_JOB_ERROR(10107, "query job execution result error", "查看作业执行结果失败"),
    SQL_CONSOLE_ENGINE_CONN_FAILED_ERROR(10108, "Fail to getConnection", "连接失败"),
    SQL_CONSOLE_MONITOR_LIST_ERROR(10109, "query monitor list result error", "查看作业执行历史记录失败"),
    SQL_CONSOLE_EXEC_SUCCESS_ABORT_ERROR(10110, "execute successful, can not be aborted", "执行成功，无法中断"),
    SQL_CONSOLE_EXEC_FAILED_ABORT_ERROR(10110, "execute failed, can not be aborted", "执行失败，无法中断"),
    SQL_CONSOLE_ABORT_ERROR(10110, "abort failed", "中断失败"),

    METADATA_USER_DBS_ERROR(10201, "get user databases error", "获得用户数据库信息失败"),
    METADATA_QUERY_LIST_PAGING_ERROR(10015, "query metadata list paging error", "分页查询元数据列表错误"),

    USER_NOT_EXIST(10010, "user {0} not exists", "用户[{0}]不存在"),
    CREATE_USER_ERROR(10011, "create user error", "创建用户错误"),
    UPDATE_USER_ERROR(10012, "update user error", "更新用户错误"),
    DELETE_USER_BY_ID_ERROR(10013, "delete user by id error", "删除用户错误"),
    USER_LIST_ERROR(10014, "user list error", "查询用户列表错误"),
    QUERY_USER_LIST_PAGING_ERROR(10015, "query user list paging error", "分页查询用户列表错误"),
    USER_NAME_EXIST(10016, "user name already exists", "用户名已存在"),
    NOT_ALLOW_TO_DISABLE_OWN_ACCOUNT(10017, "Not allow to disable your own account", "不能停用自己的账号"),
    GET_USER_INFO_ERROR(10018, "get user info error", "获取用户信息错误"),
    VERIFY_USERNAME_ERROR(10019, "verify username error", "用户名验证错误"),
    BATCH_DELETE_USER_INSTANCE_BY_IDS_ERROR(10020, "batch delete user instance by ids {0} error", "批量删除用户实例错误: {0}"),
    USER_NAME_NULL(10021, "user name is null", "用户名不能为空"),
    USER_LOGIN_FAILURE(10022, "user login failure", "用户登录失败"),
    USER_NAME_PASSWD_ERROR(10023, "user name or password error", "用户名或密码错误"),
    LOGIN_SUCCESS(10024, "login success", "登录成功"),
    SIGN_OUT_ERROR(10025, "sign out error", "退出错误"),
    IP_IS_EMPTY(10026, "ip is empty", "IP地址不能为空"),
    LOGIN_SESSION_FAILED(10027, "create session failed!", "创建session失败"),

    CREATE_ACCESS_TOKEN_ERROR(10201, "create access token error", "创建访问token错误"),
    GENERATE_TOKEN_ERROR(10202, "generate token error", "生成token错误"),
    QUERY_ACCESSTOKEN_LIST_PAGING_ERROR(10203, "query access token list paging error", "分页查询访问token列表错误"),
    UPDATE_ACCESS_TOKEN_ERROR(10204, "update access token error", "更新访问token错误"),
    DELETE_ACCESS_TOKEN_ERROR(10205, "delete access token error", "删除访问token错误"),
    ACCESS_TOKEN_NOT_EXIST(10206, "access token not exist", "访问token不存在"),
    QUERY_ACCESSTOKEN_BY_USER_ERROR(10207, "query access token by user error", "查询访问指定用户的token错误"),

    DATASOURCE_NAME_EXIST(10301, "datasource name already exists", "数据源名称已存在"),
    DATASOURCE_NOT_EXIST(10302, "datasource not exists", "数据源不存在"),
    DATASOURCE_TEST_FAILED(10303, "datasource test connection failed", "数据源测试连接失败"),

    JOB_NAME_EXIST(10401, "job name already exists", "任务名称已存在"),
    JOB_NOT_EXIST(10402, "job not exists", "任务不存在"),
    JOB_SCHEDULE_STATUS(10403, "job schedule status is {0}", "任务状态为[{0}]"),
    JOB_SCHEDULE_TIME_ERR(10404, "job schedule time err", "任务调度时间错误"),
    JOB_SCHEDULE_OFF_ERR(10405, "job schedule off err", "任务调度关闭错误"),

    USER_NO_OPERATION_PERM(30001, "user has no operation privilege", "当前用户没有操作权限");

    private final int code;
    private final String enMessage;
    private final String zhMessage;

    Status(int code, String enMessage, String zhMessage) {
        this.code = code;
        this.enMessage = enMessage;
        this.zhMessage = zhMessage;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        if (Locale.SIMPLIFIED_CHINESE.getLanguage().equals(LocaleContextHolder.getLocale().getLanguage())) {
            return this.zhMessage;
        } else {
            return this.enMessage;
        }
    }

    /**
     * Retrieve Status enum entity by status code.
     */
    public static Optional<Status> findStatusBy(int code) {
        for (Status status : Status.values()) {
            if (code == status.getCode()) {
                return Optional.of(status);
            }
        }
        return Optional.empty();
    }
}
