/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

import lombok.Getter;

@Getter
public enum JobRunningStatusEnum {
    /**
     * 0执行中
     */
    RUNNING,
    /**
     * 1成功
     */
    SUCCEED,
    /**
     * 2失败
     */
    FAILURE,
    /**
     * 3队列中
     */
    QUEUING,
    /**
     * 4已终止
     */
    INTERRUPTED;

    public static JobRunningStatusEnum convert(int index) {
        switch (index) {
            case 0:
                return JobRunningStatusEnum.RUNNING;
            case 1:
                return JobRunningStatusEnum.SUCCEED;
            case 2:
                return JobRunningStatusEnum.FAILURE;
            case 3:
                return JobRunningStatusEnum.QUEUING;
            case 4:
                return JobRunningStatusEnum.INTERRUPTED;
            default:
                throw new IllegalStateException("Unexpected value: " + index);
        }
    }
}
