/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.enums;

@SuppressWarnings("PMD.EnumConstantsMustHaveCommentRule")
public enum Symbol {
    BLANK(" "),
    COMMA(","),
    COLON(":"),
    SEMICOLON(";"),
    AT("@"),
    STAR("*"),
    PERCENT("%"),
    ESCAPE_PERCENT("\\%"),
    UNDERSCORE("_"),
    ESCAPE_UNDERSCORE("\\_"),
    DASH("-"),
    AND("&"),
    SLASH("/"),
    BACKSLASH("\\"),
    VLINE("|"),
    NEWLINE("\n"),
    LEFT_SQUARE_BRACKETS("["),
    RIGHT_SQUARE_BRACKETS("]"),
    DOT("."),
    SHARP("#"),
    QUESTION("?"),
    EQUAL("=");

    /**
     * the symbol
     */
    private final String symbol;

    /**
     * The constructor.
     *
     * @param symbol the symbol
     */
    Symbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * Get the symbol.
     *
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

}
