/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.common.dto;

import com.chinamobile.cmss.lakehouse.common.enums.ExecuteTaskType;
import com.chinamobile.cmss.lakehouse.common.utils.DateSerializer;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class TableDto implements Serializable {
    @ApiModelProperty("table name")
    private String tableName;
    @ApiModelProperty("database name")
    private String databaseName;
    @ApiModelProperty("path")
    private String location;
    @ApiModelProperty("reference task type")
    private ExecuteTaskType refTaskType;
    @ApiModelProperty("reference task id")
    private String refTaskId;
    @ApiModelProperty("create user")
    private String creator;
    @JsonSerialize(using = DateSerializer.class)
    @ApiModelProperty("create time")
    private Integer createTime;
    @ApiModelProperty("latest modifier")
    private String lastModifier;
    @JsonSerialize(using = DateSerializer.class)
    @ApiModelProperty("latest modifier time")
    private Integer lastModifierTime;

    public Integer getLastModifierTime() {
        // if created time later than latest modifier time, use create time.
        if (lastModifierTime != null && createTime != null && createTime > lastModifierTime) {
            return createTime;
        }
        return lastModifierTime;
    }
}
