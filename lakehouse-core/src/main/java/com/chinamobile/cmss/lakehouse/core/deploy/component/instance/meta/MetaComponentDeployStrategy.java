/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.component.instance.meta;

import com.chinamobile.cmss.lakehouse.common.annotation.ComponentTypeAnno;
import com.chinamobile.cmss.lakehouse.common.enums.ComponentTypeEnum;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;
import com.chinamobile.cmss.lakehouse.core.deploy.component.descriptor.ComponentDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.component.strategy.ComponentDeployStrategy;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@ComponentTypeAnno(ComponentTypeEnum.META)
public class MetaComponentDeployStrategy extends ComponentDeployStrategy {

    @Resource
    private MetaComponentDescriptor descriptor;

    @Override
    public void deploy(String namespace, String clusterName, ComponentConfiguration config) {
        log.info("start to deploy meta component to k8s:{}", clusterName);
        super.deploy(namespace, clusterName, config);
    }

    @Override
    protected ComponentDescriptor descriptor() {
        return descriptor;
    }

}
