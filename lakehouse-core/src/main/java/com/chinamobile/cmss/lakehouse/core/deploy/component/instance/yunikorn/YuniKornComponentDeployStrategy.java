/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.component.instance.yunikorn;

import com.chinamobile.cmss.lakehouse.common.annotation.ComponentTypeAnno;
import com.chinamobile.cmss.lakehouse.common.enums.ComponentTypeEnum;
import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant;
import com.chinamobile.cmss.lakehouse.core.client.InformerException;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;
import com.chinamobile.cmss.lakehouse.core.deploy.component.descriptor.ComponentDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.component.strategy.ComponentDeployStrategy;

import javax.annotation.Resource;

import io.kubernetes.client.openapi.models.V1ClusterRoleBinding;
import io.kubernetes.client.openapi.models.V1ClusterRoleBindingBuilder;
import io.kubernetes.client.openapi.models.V1ObjectMetaBuilder;
import io.kubernetes.client.openapi.models.V1RoleRef;
import io.kubernetes.client.openapi.models.V1Subject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@ComponentTypeAnno(ComponentTypeEnum.YUNIKORN)
@Slf4j
public class YuniKornComponentDeployStrategy extends ComponentDeployStrategy {

    private String clusterRoleBindingName = "yunikorn-rbac";
    private String subjectName = "system:serviceaccounts";
    private String roleRefName = "cluster-admin";

    @Resource
    private YuniKornComponentDescriptor descriptor;

    @Override
    public void deploy(String namespace, String clusterName, ComponentConfiguration config) {
        log.info("start to deploy YuniKorn component to k8s:{}", clusterName);

        // create clusterRoleBinding firstly
        try {
            deployHandler.createClusterRoleBinding(buildClusterRoleBinding());
        } catch (InformerException e) {
            throw new BaseException(e);
        }

        // deploy yunikorn cluster
        super.deploy(namespace, clusterName, config);
    }

    @Override
    protected ComponentDescriptor descriptor() {
        return descriptor;
    }

    private V1ClusterRoleBinding buildClusterRoleBinding() {
        return new V1ClusterRoleBindingBuilder().withApiVersion(K8sModelConstant.RBAC_API_VERSION)
            .withKind(K8sModelConstant.CLUSTER_ROLE_BINDING_KIND)
            .withMetadata(new V1ObjectMetaBuilder().withName(clusterRoleBindingName)
                .withLabels(K8sModelConstant.NAMESPACE_LABELS).build())
            .withSubjects(new V1Subject().kind(K8sModelConstant.SUBJECT_KIND).name(subjectName)
                .apiGroup(K8sModelConstant.RBAC_API_GROUP))
            .withRoleRef(new V1RoleRef().kind(K8sModelConstant.CLUSTER_ROLE_KIND).name(roleRefName)
                .apiGroup(K8sModelConstant.RBAC_API_GROUP))
            .build();
    }

}
