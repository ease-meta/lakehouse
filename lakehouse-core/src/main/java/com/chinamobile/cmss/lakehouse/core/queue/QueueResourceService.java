/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.queue;

import com.chinamobile.cmss.lakehouse.common.dto.ResourceCheckResult;
import com.chinamobile.cmss.lakehouse.common.dto.yunikorn.QueuesCapacity;
import com.chinamobile.cmss.lakehouse.common.enums.ResourceSizeEnum;

public interface QueueResourceService {

    ResourceCheckResult runnable(String queue, int cpu, int memory);

    boolean createYunikornQueue(String queueName, ResourceSizeEnum resourceSize);

    ResourceCheckResult resizeYunikornQueue(String queueName, ResourceSizeEnum resourceSize);

    boolean deleteYunikornQueue(String queueName);

    ResourceCheckResult checkResizeYunikornQueue(String queueName, ResourceSizeEnum resourceSize);

    ResourceCheckResult checkCreateYunikornQueue(String queueName, ResourceSizeEnum resourceSize);

    boolean checkExists(String queueName);

    QueuesCapacity getQueueCapacity();

}
