/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.component.strategy;

import com.chinamobile.cmss.lakehouse.common.exception.BaseException;
import com.chinamobile.cmss.lakehouse.core.client.InformerException;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;
import com.chinamobile.cmss.lakehouse.core.deploy.component.descriptor.ComponentDescriptor;
import com.chinamobile.cmss.lakehouse.core.handler.K8sDeployHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.kubernetes.client.common.KubernetesObject;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ComponentDeployStrategy {

    @Autowired
    protected K8sDeployHandler deployHandler;

    protected void deploy(String namespace, String clusterName, ComponentConfiguration config) {
        try {
            List<KubernetesObject> k8sObjs = new ArrayList<>();
            descriptor().buildConfigMap(config).ifPresent(k8sObjs::addAll);
            descriptor().buildService(config).ifPresent(k8sObjs::addAll);
            descriptor().buildStatefulSet(config).ifPresent(k8sObjs::add);
            descriptor().buildDeployment(config).ifPresent(k8sObjs::add);
            deployHandler.deploy(namespace, k8sObjs);
        } catch (IOException | InformerException e) {
            throw new BaseException(e);
        }
    }

    protected abstract ComponentDescriptor descriptor();

}