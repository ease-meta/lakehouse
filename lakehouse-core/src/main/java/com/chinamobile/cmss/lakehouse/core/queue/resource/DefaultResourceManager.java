/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.queue.resource;

import com.chinamobile.cmss.lakehouse.common.dto.ResourceCheckResult;
import com.chinamobile.cmss.lakehouse.common.dto.yunikorn.ConfigQueue;
import com.chinamobile.cmss.lakehouse.common.dto.yunikorn.ConfigResources;
import com.chinamobile.cmss.lakehouse.common.dto.yunikorn.QueuesCapacity;
import com.chinamobile.cmss.lakehouse.core.queue.ResourceManager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DefaultResourceManager implements ResourceManager {
    @Override
    public boolean runnable(String queue, int cpu, int memory) {
        return true;
    }

    @Override
    public void createQueue(ConfigQueue configQueue) {

    }

    @Override
    public void deleteQueue(String queueName) {

    }

    @Override
    public void editConfigQueue(String queueName, ConfigResources configResources) {

    }

    @Override
    public ResourceCheckResult checkEditQueue(String queueName, ConfigResources configResources) {
        return null;
    }

    @Override
    public ResourceCheckResult checkCreateQueue(ConfigQueue configQueue) {
        return null;
    }

    @Override
    public boolean existsQueueName(String queueName) {
        return false;
    }

    @Override
    public QueuesCapacity getQueuesCapacity() {
        return null;
    }
}
