/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.cluster.descriptor;

import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.CLUSTER_NAMESPACE_PREFIX;
import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.COMMON_NAMESPACE_SUFFIX;
import static com.chinamobile.cmss.lakehouse.common.kubernetes.K8sModelConstant.NAMESPACE_SEPARATOR;

import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;

import java.util.List;

public abstract class ClusterDescriptor {

    public String commonNamespace() {
        return CLUSTER_NAMESPACE_PREFIX + NAMESPACE_SEPARATOR + COMMON_NAMESPACE_SUFFIX;
    }

    public String namespace(String clusterName) {
        return CLUSTER_NAMESPACE_PREFIX + NAMESPACE_SEPARATOR + clusterName.replace("_", "-");
    }

    public abstract List<ClusterComponentSpec> clusterComponentSpecs();

    public abstract ComponentConfiguration buildComponentConfig(LakehouseCreateRequest request);

    public abstract boolean isCommonRunning();

    public abstract boolean isCommonResourceExisting();

    public boolean isRunning(String namespace) {
        return false;
    }
}
