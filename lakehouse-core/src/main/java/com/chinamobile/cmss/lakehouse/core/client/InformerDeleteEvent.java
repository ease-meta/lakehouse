/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.client;

import java.util.concurrent.CountDownLatch;

import lombok.Data;

@Data
public class InformerDeleteEvent<ApiType> {
    private String name;
    private InformerEventType type;
    private DeleteStatus status;
    private InformerException exception;
    private CountDownLatch latch;
    private ApiType result;

    private InformerDeleteEvent(String name, InformerEventType eventType) {
        this.name = name;
        this.type = eventType;
        this.status = DeleteStatus.NOT_OPERATE;
        this.latch = new CountDownLatch(1);
    }

    public static InformerDeleteEvent create(String name, InformerEventType eventType) {
        return new InformerDeleteEvent(name, eventType);
    }

    public void release() {
        this.latch.countDown();
    }

    public enum DeleteStatus {
        /**
         * no operate
         */
        NOT_OPERATE,

        /**
         * delete success
         */
        DELETE_SUCCESS,

        /**
         * delete error
         */
        DELETE_ERROR
    }
}
