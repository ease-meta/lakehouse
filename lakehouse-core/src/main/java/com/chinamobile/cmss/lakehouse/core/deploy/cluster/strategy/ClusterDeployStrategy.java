/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.deploy.cluster.strategy;

import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.core.config.ComponentConfiguration;
import com.chinamobile.cmss.lakehouse.core.deploy.cluster.descriptor.ClusterComponentSpec;
import com.chinamobile.cmss.lakehouse.core.deploy.cluster.descriptor.ClusterDescriptor;
import com.chinamobile.cmss.lakehouse.core.deploy.component.strategy.ComponentDeployStrategyContext;

import javax.annotation.Resource;

public abstract class ClusterDeployStrategy {

    @Resource
    private ComponentDeployStrategyContext componentDeployStrategyContext;

    public void deploy(LakehouseCreateRequest request) {
        String instance = request.getInstance();
        ClusterDescriptor descriptor = descriptor();
        ComponentConfiguration config = descriptor.buildComponentConfig(request);

        for (ClusterComponentSpec spec : descriptor.clusterComponentSpecs()) {
            // deploy common component if resources are not existing
            if (spec.isCommon() && !descriptor.isCommonResourceExisting()) {
                componentDeployStrategyContext.deploy(descriptor.commonNamespace(), spec.getComponentType(),
                    instance, config);
            }
        }

    }

    protected abstract ClusterDescriptor descriptor();

}