/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.pipeline;

import com.chinamobile.cmss.lakehouse.common.dto.LakehouseCreateRequest;
import com.chinamobile.cmss.lakehouse.core.client.InformerClient;
import com.chinamobile.cmss.lakehouse.core.client.impl.KubernetesClientImpl;
import com.chinamobile.cmss.lakehouse.core.handler.K8sServiceHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NamespaceOpsHandler implements CreateHandler<ClusterCreateContext>,

    ReleaseHandler<ClusterReleaseContext>, CheckHandler<ClusterCheckContext> {

    @Autowired
    private InformerClient informerClient;

    @Override
    public boolean handleCreate(ClusterCreateContext context) {
        LakehouseCreateRequest request = context.getRequest();
        String instance = request.getInstance();
        String namespace = K8sServiceHandler.clusterNamespace(instance);
        try {
            informerClient.createNamespace(KubernetesClientImpl.buildNamespace(namespace));
        } catch (Exception e) {
            log.error("Failed to create namespace {}", namespace, e);
            return false;
        }
        return true;
    }

    @Override
    public boolean handleCheck(ClusterCheckContext context) {
        return false;
    }

    @Override
    public boolean handleRelease(ClusterReleaseContext context) {
        return false;
    }
}

