/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.config;

/**
 * A ConfigOption contains the key and an optional default value.
 *
 * @param <V> The type of the default value.
 */
public class ConfigOption<V> {

    private final String key;

    private final V defaultValue;

    ConfigOption(String key, V defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    /**
     * Create a OptionBuilder with key
     *
     * @param key The key of the option
     * @return Return a OptionBuilder.
     */
    public static OptionBuilder key(String key) {
        return new OptionBuilder(key);
    }

    public String key() {
        return key;
    }

    public V defaultValue() {
        return defaultValue;
    }

    /**
     * The OptionBuilder is used to build the ConfigOption.
     */
    public static final class OptionBuilder {

        private final String key;

        OptionBuilder(String key) {
            this.key = key;
        }

        public <V> ConfigOption<V> defaultValue(V value) {
            return new ConfigOption<>(key, value);
        }
    }
}
