/*
 * Copyright (c) 2022. China Mobile (SuZhou) Software Technology Co.,Ltd. All rights reserved.
 * Lakehouse is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package com.chinamobile.cmss.lakehouse.core.client;

import java.util.concurrent.CountDownLatch;

import lombok.Data;

@Data
public class InformerCreateEvent<ApiType> {
    private String name;
    private InformerEventType type;
    private CreateStatus status;
    private InformerException exception;
    private CountDownLatch latch;
    private ApiType result;

    private InformerCreateEvent(String name, InformerEventType eventType) {
        this.name = name;
        this.type = eventType;
        this.status = CreateStatus.NOT_OPERATE;
        this.latch = new CountDownLatch(1);
    }

    public static InformerCreateEvent create(String name, InformerEventType eventType) {
        return new InformerCreateEvent(name, eventType);
    }

    public void release() {
        this.latch.countDown();
    }

    public enum CreateStatus {
        /**
         * not operate
         */
        NOT_OPERATE,

        /**
         * create success
         */
        CREATE_SUCCESS,

        /**
         * create error
         */
        CREATE_ERROR
    }
}
